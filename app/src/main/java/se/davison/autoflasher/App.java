package se.davison.autoflasher;

import android.app.Application;
import android.util.Log;

import com.github.lukaspili.reactivebilling.Logger;
import com.github.lukaspili.reactivebilling.ReactiveBilling;


/**
 * Created by richard on 2017-02-17.
 */

public class App extends Application {


    private static final String TAG = "ReacitveBillling";

    @Override
    public void onCreate() {
        super.onCreate();

        ReactiveBilling.getInstance(this).setLogger(new Logger() {
            @Override
            public void log(String s) {
                Log.d(TAG,s);
            }
        });
    }
}
