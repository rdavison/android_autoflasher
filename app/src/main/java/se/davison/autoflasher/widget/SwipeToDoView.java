package se.davison.autoflasher.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Objects;

import se.davison.autoflasher.R;

/**
 * Created by richard on 20/01/15.
 */
public class SwipeToDoView extends RelativeLayout {


    private static final String TAG = SwipeToDoView.class.getSimpleName();
    private final ImageView imgSwipe;
    private ColorStateList textColors;
    private int initAlpha;
    private final TextView textView;
    private Drawable background;
    private Drawable swipeBackground;
    private Object lock = new Object();

    private boolean runAnimation = true;
    private Animation animation;
    private boolean touching = false;
    private boolean runCallback = false;
    private boolean swiped = false;


    @Override
    public void setEnabled(boolean enabled) {
        if (background == null) {
            background = getBackground();
            swipeBackground = imgSwipe.getBackground();
        }
        if (enabled) {
            swipeBackground.setAlpha(255);
            background.setAlpha(255);
        } else {
            swipeBackground.setAlpha(85);
            background.setAlpha(85);
        }
        super.setEnabled(enabled);

    }

    private int downX = 0;
    private OnSwipedListener swipedListener;

    public interface OnSwipedListener {
        void onSwiped();
    }

    public void setTextColor(int color) {
        textView.setTextColor(color);
        textColors = textView.getTextColors();
        initAlpha = (textColors.getDefaultColor() & 0xFF000000) >>> 24;
    }

    public void setOnSwipedListener(OnSwipedListener swipedListener) {
        this.swipedListener = swipedListener;
    }

    public SwipeToDoView(final Context context, final AttributeSet attrs) {
        super(context, attrs);


        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.SwipeToDoView,
                0, 0);

        int textColor = a.getColor(R.styleable.SwipeToDoView_textColor,Color.WHITE);
        int iconColor = a.getColor(R.styleable.SwipeToDoView_color,Color.WHITE);
        int backgroundColor = a.getColor(R.styleable.SwipeToDoView_background,Color.BLACK);
        Drawable src = a.getDrawable(R.styleable.SwipeToDoView_src);
        String text = a.getString(R.styleable.SwipeToDoView_text);

        a.recycle();


        imgSwipe = new ImageView(context);
        imgSwipe.setId(android.R.id.icon);
        imgSwipe.setBackgroundColor(iconColor);
        if (src != null) {
            imgSwipe.setImageDrawable(src);
        }


        textView = new TextView(context);
        setTextColor(textColor);
        textView.setText(text.toUpperCase());
        textView.setGravity(Gravity.CENTER);

        RelativeLayout.LayoutParams textParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
        textParams.addRule(RelativeLayout.ALIGN_TOP, imgSwipe.getId());
        textParams.addRule(RelativeLayout.ALIGN_BOTTOM, imgSwipe.getId());
        RelativeLayout.LayoutParams imgParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        addView(textView, 0, textParams);
        addView(imgSwipe, 1, imgParams);
        OnTouchListener onTouchListener = new OnTouchListener() {

            //MarginLayoutParams params;
            private int totalWidth = 0;


            public int alpha(int margin) {
                return (int) Math.max(0, ((float) initAlpha) * (1 - ((float) margin / ((float) totalWidth * 0.33f))));
            }


            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (!isEnabled()) {
                    return false;
                }

                if (!swiped) {


                    switch (event.getAction()) {
                        case MotionEvent.ACTION_MOVE:

                            if (touching) {
                                int margin = (int) event.getX() - downX;
                                if (margin <= 0) {
                                    margin = 0;
                                }
                                if (margin >= totalWidth) {
                                    margin = totalWidth;
                                    swiped = true;
                                }
                                if (animation != null) {
                                    animation.cancel();
                                }


                                textView.setTextColor(textColors.withAlpha(alpha(margin)));

                                MarginLayoutParams params = (MarginLayoutParams) imgSwipe.getLayoutParams();
                                params.setMargins(margin, 0, 0, 0);
                                imgSwipe.setLayoutParams(params);
                                if (swiped && swipedListener != null && !runCallback) {
                                    swiped = false;
                                    runCallback = true;
                                    swipedListener.onSwiped();
                                    runAnimation = false;
                                }
                            }
                            break;
                        case MotionEvent.ACTION_DOWN:

                            totalWidth = SwipeToDoView.this.getWidth() - imgSwipe.getWidth();

                            downX = (int) event.getX();
                            if (isViewContains(imgSwipe, (int) event.getX(), (int) event.getY())) {
                                touching = true;
                            }
                            break;
                        case MotionEvent.ACTION_UP:

                            if (touching && runAnimation) {
                                runResetAnimation((int) event.getX());
                            }
                            if (!runAnimation) {
                                runAnimation = true;
                            }
                            break;
                    }
                }
                return true;
            }
        };
        this.setOnTouchListener(onTouchListener);


    }

    private void runResetAnimation(int x) {
        final int margin = x - downX;


        textView.setTextColor(textColors);


        animation = new MarginAnimation(imgSwipe, 0, 0, 0, 0);
        animation.setDuration(600);
        animation.setInterpolator(new BounceInterpolator());
        imgSwipe.startAnimation(animation);
        touching = false;
        runCallback = false;

    }

    public void reset() {
        swiped = false;
        runAnimation = true;
        runResetAnimation(0);
    }

    private static class MarginAnimation extends Animation {

        private View target;
        private int left;
        private int top;
        private int right;
        private int bottom;

        private int curMarginLeft = 0;
        private int curMarginTop = 0;
        private int curMarginRight = 0;
        private int curMarginBottom = 0;


        private MarginAnimation(View target, int left, int top, int right, int bottom) {
            this.target = target;
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
        }

        @Override
        public void initialize(int width, int height, int parentWidth, int parentHeight) {
            super.initialize(width, height, parentWidth, parentHeight);

            MarginLayoutParams params = (MarginLayoutParams) target.getLayoutParams();
            curMarginLeft = params.leftMargin;
            curMarginTop = params.topMargin;
            curMarginRight = params.rightMargin;
            curMarginBottom = params.bottomMargin;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            MarginLayoutParams params = (MarginLayoutParams) target.getLayoutParams();
            params.setMargins(getValue(interpolatedTime, left, curMarginLeft), getValue(interpolatedTime, top, curMarginTop), getValue(interpolatedTime, right, curMarginRight), getValue(interpolatedTime, bottom, curMarginBottom));
            target.setLayoutParams(params);

        }

        private int getValue(float interpolatedTime, int target, int current) {
            return current - (int) (((float) (current - target)) * interpolatedTime);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        //Animation bounce = new Bounce
        //imgSwipe.startAnimation();
    }

    private boolean isViewContains(View view, int rx, int ry) {
        int[] l = new int[2];


        //view.getLocationOnScreen(l);
        int x = view.getLeft();
        int y = view.getTop();
        int w = view.getWidth();
        int h = view.getHeight();

        return !(rx < x || rx > x + w || ry < y || ry > y + h);
    }

    public SwipeToDoView(Context context) {
        this(context, null);
    }
}
