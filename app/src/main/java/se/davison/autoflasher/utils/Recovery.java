package se.davison.autoflasher.utils;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;


import com.stericson.RootShell.exceptions.RootDeniedException;
import com.stericson.RootShell.execution.Command;
import com.stericson.RootTools.RootTools;

import android.os.Environment;
import android.util.Log;

public class Recovery {
	
	private static final FileFilter FOLDER_FILTER = new FileFilter() {
		@Override
		public boolean accept(File pathname) {
			return pathname != null && pathname.isDirectory();
		}
	};
	
	public static final SimpleDateFormat BACKUP_DATE_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd HH.mm.ss");
	
	
	public static final int TWRP = 0;
	public static final int CWM = 1;
	
	private static final String TAG = Recovery.class.getSimpleName();
	private static final String BACKUP_NAME = "backup";
	private final String recoveryName;
	private int recovery = TWRP;
	
	private boolean wipeData = false;
	private boolean backup = false;
	private boolean wipeCache = false;
	private boolean wipeSystem = false;
	private boolean formatSystemF2fs = false;
	private File eraseImageBinary = null;
	
	private List<File> flashFiles;
	private File tempDirectory;
	private Map<String, Boolean> backOptions;
	
	public Recovery wipeData() {
		wipeData = true;
		return this;
	}
	
	public Recovery wipeCache() {
		wipeCache = true;
		return this;
	}
	
	public Recovery backup() {
		backup = true;
		return this;
	}
	
	public void setTempDirectory(File tempDirectory) {
		this.tempDirectory = tempDirectory;
	}
	
	public Recovery(int recovery) {
		this.recovery = recovery;
		recoveryName = this.recovery == TWRP ? "openrecoveryscript" : "command";
	}
	
	public static Recovery cwm() {
		return new Recovery(CWM);
	}
	
	public static Recovery twrp() {
		return new Recovery(TWRP);
	}
	
	public Recovery files(File... files) {
		flashFiles = new ArrayList<File>();
		if (recovery == CWM) {
			flashFiles.add(files[0]);
		} else {
			flashFiles.addAll(Arrays.asList(files));
		}
		return this;
	}
	
	private static String getExternalPath() {
		return Environment.getExternalStorageDirectory().getPath() + "/";
	}
	
	public void setEraseImageBinary(File eraseImageBinary) {
		if (eraseImageBinary.exists()) {
			this.eraseImageBinary = eraseImageBinary;
		}
	}
	
	public static void deleteDirectory(File directory) {
		if (directory.exists()) {
			String deleteCmd = "rm -r -f "
					+ directory.getAbsolutePath();
			Runtime runtime = Runtime.getRuntime();
			try {
				Process proc = runtime.exec(deleteCmd);
				proc.waitFor();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void deleteOldBackup() {
		File backupRoot = new File(getExternalPath() + "TWRP/BACKUPS/");
		if (backupRoot.exists()) {
			File[] backupList = backupRoot.listFiles(FOLDER_FILTER);
			if (backupList != null) {
				for (File currentBackupFolder : backupList) {
					File[] subBackupFolders = currentBackupFolder
							.listFiles(FOLDER_FILTER);
					if (subBackupFolders != null) {
						for (File backupFolder : subBackupFolders) {
							if (backupFolder.getName().equals(
									"autoflasherbackup")) {
								deleteDirectory(backupFolder);
								return;
							}
						}
					}
				}
			}
		}
	}


//    private String appendFile(String fileName, String text){
//        return "echo '"+text+"' >>"+fileName;
//    }
	
	public void flash() throws RootDeniedException, IOException, TimeoutException {
		if (RootTools.isAccessGiven()) {
			
			List<String> commands = new ArrayList<String>();
			commands.add("mkdir -p /cache/recovery/");
			
			String extCmd = "";
			
			boolean twrp = recovery == TWRP;
			
			
			if (backup) {
				deleteOldBackup();
				if (twrp) {
					extCmd += "backup" + backupOptionsValues() + " " + BACKUP_NAME + "_"
							+ BACKUP_DATE_FORMAT.format(new Date()) + "\n";
				}
			}
			
			if (wipeData) {
				if (twrp) {
					extCmd += "wipe data\n";
				} else {
					extCmd += "--wipe_data\n";
				}
			}
			
			if (wipeSystem && twrp && eraseImageBinary != null) {
				extCmd += "install " + eraseImageBinary.getAbsolutePath() + "\n";
				//extCmd += "cmd chmod 777 "+eraseImageBinary.getAbsolutePath()+"\n";
				//extCmd += "cmd erase_image system\n";
				//extCmd += "cmd rm -rfv system/*\n";
			}
			
			for (File f : flashFiles) {
				if (twrp) {
					extCmd += "install " + f.getAbsolutePath() + "\n";
				} else {
					extCmd += "--update_package=\"" + f.getAbsolutePath() + "\"\n";
					break;
				}
			}
			
			if (formatSystemF2fs && twrp) {
				extCmd += "backup S /sdcard/tmp/system\n";
				extCmd += "set tw_rm_rf 1\n";
				extCmd += "cmd mkfs.f2fs system\n";
				extCmd += "restore S /sdcard/tmp/system\n";
				extCmd += "cmd rm -rf /sdcard/tmp/system\n";
			}
			
			
			if (wipeCache) {
				if (twrp) {
					extCmd += "wipe cache\n";
					extCmd += "wipe dalvik\n";
				} else {
					extCmd += "--wipe_cache\n";
				}
			}
			
			
			if (twrp && tempDirectory != null) {
				extCmd += "cmd rm -f -r " + tempDirectory.getAbsolutePath() + "\n";
			}
			
			
			Log.d(TAG, "===FLASH COMMAND===");
			Log.d(TAG, extCmd);
			
			commands.add("echo '" + extCmd + "' >/cache/recovery/" + recoveryName);
			commands.add("reboot recovery");
			
			int size = commands.size();
			String[] arrCom = new String[size];
			for (int i = 0; i < size; i++) {
				arrCom[i] = commands.get(i);
			}
			
			
			Command command = new Command(0, arrCom);
			RootTools.getShell(true).add(command).finish();
		} else {
			throw new RootDeniedException("Root is denied");
		}
		
	}
	
	private String backupOptionsValues() {
		if (backOptions != null && backOptions.size() > 0) {
			String options = " ";
			for (String key : backOptions.keySet()) {
				if (backOptions.get(key)) {
					options += key;
				}
			}
			return options;
		} else {
			return " SDBO";
		}
	}
	
	
	public static void sudo(String... strings) throws IOException, InterruptedException {
		Process su = Runtime.getRuntime().exec("su");
		DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());
		
		for (String s : strings) {
			outputStream.writeBytes(s + "\n");
			outputStream.flush();
		}
		
		outputStream.writeBytes("exit\n");
		outputStream.flush();
		su.waitFor();
		outputStream.close();
		su.destroy();
		
	}
	
	public void clean() throws TimeoutException, RootDeniedException, IOException {
		Command command = new Command(0, "rm -f /cache/recovery/" + recoveryName);
		RootTools.getShell(true).add(command).finish();
	}
	
	public void setBackup(boolean backup, Map<String, Boolean> backupOptions) {
		this.backup = backup;
		this.backOptions = backupOptions;
	}
	
	public void setWipeData(boolean wipeData) {
		this.wipeData = wipeData;
	}
	
	public void setWipeCache(boolean wipeCache) {
		this.wipeCache = wipeCache;
	}
	
	public boolean isWipeCache() {
		return wipeCache;
	}
	
	public void setWipeSystem(boolean wipeSystem) {
		this.wipeSystem = wipeSystem;
	}
	
	public void setFormatSystemF2fs(boolean formatSystemF2fs) {
		this.formatSystemF2fs = formatSystemF2fs;
	}
	
	public boolean isFormatSystemF2fs() {
		return formatSystemF2fs;
	}
}
