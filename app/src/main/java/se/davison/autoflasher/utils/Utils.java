package se.davison.autoflasher.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.provider.Settings;
import android.util.Base64;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import se.davison.autoflasher.BuildConfig;

/**
 * Created by richard on 18/01/15.
 */
public class Utils {

    private static String PREF_KEY_PREMIUM = "pref_key_premium";

    public static int dpToPx(int dp, Context context) {
        return (int) context.getResources().getDisplayMetrics().density * dp;
    }

    public static void setupAds(AdView adView, boolean isPremium){
        if(isPremium) {
            adView.setVisibility(View.INVISIBLE);
            adView.destroy();
        }else{

            AdRequest.Builder reqBuilder = new AdRequest.Builder().addKeyword("flash").addKeyword("root").addKeyword("android").addKeyword("custom rom").addKeyword("technical").addKeyword("development");
            if (!BuildConfig.DEBUG) {
                //adView.setAdUnitId(adId);
            }else{
                reqBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
                //adView.setAdUnitId("dummy");
            }
            //adView.setAdSize(AdSize.BANNER);
            adView.loadAd(reqBuilder.build());
            adView.setVisibility(View.VISIBLE);
        }
    }

    private static String getSecureId(Context context){
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static boolean isPremium(SharedPreferences prefs, Context context){
        String premium = prefs.getString(PREF_KEY_PREMIUM, null);

        if(premium == null){
            return false;
        }
        premium = premium.trim();
        String hash = Utils.SHA256("premium", getSecureId(context)).trim();
        return premium.equals(hash);
    }

    @SuppressLint("CommitPrefEdits")
    public static void setPremium(SharedPreferences prefs, Context context, boolean isPremium){
        prefs.edit().putString(PREF_KEY_PREMIUM, isPremium ? Utils.SHA256("premium", getSecureId(context)).trim() : null).commit();
    }

    public static String SHA256(String text, String salt){
        byte[] digest = (text+salt).getBytes();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(digest);
            digest = md.digest();
        }catch (NoSuchAlgorithmException ex){
        }

        return Base64.encodeToString(digest, Base64.DEFAULT);
    }

    public static String exceptionToString(Context context, Exception exception) {
        return "";
    }

    public static boolean isApi(int apiLevel) {
        return Build.VERSION.SDK_INT >= apiLevel;
    }
}
