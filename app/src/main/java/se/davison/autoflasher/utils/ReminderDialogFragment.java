package se.davison.autoflasher.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import java.lang.reflect.ReflectPermission;

/**
 * Created by davisonr on 4/3/2015.
 */
public class ReminderDialogFragment extends DialogFragment {


    private static final String EXTRA_MESSAGE = "extra_message";
    private static final String EXTRA_TITLE = "extra_title";
    private static final String EXTRA_OK_TITLE = "extra_ok_title";
    private static final String EXTRA_DISMISS_TITLE = "extra_dismiss_title";
    private static final String EXTRA_SHOW_AGAIN_TEXT = "extra_show_again_text";
    private static final String PREF_PREFIX = "pref_key_reminder_dialog_";
    private Callback callback;
    private SharedPreferences prefs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    @Override
    public void onDetach() {
        callback = null;
        super.onDetach();
    }

    @Override
    public void onAttach(Activity activity) {
        if(activity instanceof Callback){
            callback = (Callback) activity;
        }else{
            Log.e(ReminderDialogFragment.class.getSimpleName(), "Dialog not sending callbacks to activity");
        }
        super.onAttach(activity);
    }

    public interface Callback{
        void onReminderDialogAccept(String tag);
        void onReminderDialogDismiss(String tag);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle extras = getArguments();
        String title = extras.getString(EXTRA_TITLE);
        String message = extras.getString(EXTRA_MESSAGE);
        String okTitle = extras.getString(EXTRA_OK_TITLE);
        String dismissTitle = extras.getString(EXTRA_DISMISS_TITLE);
        String showAgainText = extras.getString(EXTRA_SHOW_AGAIN_TEXT);

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which == Dialog.BUTTON_POSITIVE) {
                    setShowEnabled(false);
                    callback.onReminderDialogAccept(getTag());
                }else{
                    callback.onReminderDialogDismiss(getTag());
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message);
        if(title!=null){
            builder.setTitle(title);
        }
        if(okTitle!=null){
            builder.setPositiveButton(okTitle,listener);
        }
        if(dismissTitle!=null){
            builder.setNegativeButton(dismissTitle, listener);
        }
        if(showAgainText!=null) {

            float density = getActivity().getResources().getDisplayMetrics().density;

            LinearLayout ll = new LinearLayout(getActivity());
            ll.setOrientation(LinearLayout.HORIZONTAL);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.RIGHT;


            CheckBox checkbox = new CheckBox(getActivity());
            checkbox.setText(showAgainText);

            int padding = Build.VERSION.SDK_INT<Build.VERSION_CODES.LOLLIPOP?(int)(density*8f):(int)(density*16f);
            int paddingTop = Build.VERSION.SDK_INT<Build.VERSION_CODES.LOLLIPOP?0:(int)(density*16f);


            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    setShowEnabled(!isChecked);
                }
            });
            //checkbox.setGravity(Gravity.RIGHT);
            ll.setPadding(padding,paddingTop,padding,0);
            ll.addView(checkbox, params);

            builder.setView(ll);
        }



        return builder.create();
    }

    private void setShowEnabled(boolean enabled) {
        prefs.edit().putBoolean(PREF_PREFIX+"_bool_"+getTag(), enabled).commit();
    }

    public static class Builder{




        public Builder(Context context, FragmentManager fragmentManager, String tag, int launchCount) {
            this.fm = fragmentManager;
            this.tag = tag;
            this.launchCount = launchCount;
            this.context = context;
        }


        private final String tag;
        private final FragmentManager fm;
        private final int launchCount;
        private final Context context;
        private String title;
        private String message;
        private String okTitle;
        private String dismissTitle;
        private String showAgainText;
        private int repeatAfter = -1;


        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder okTitle(String okTitle) {
            this.okTitle = okTitle;
            return this;
        }

        public Builder dismissTitle(String dismissTitle) {
            this.dismissTitle = dismissTitle;
            return this;
        }

        public Builder repeat(int repeatAfter, String showAgainText) {
            this.showAgainText = showAgainText;
            this.repeatAfter = repeatAfter;
            return this;
        }

        public void show(){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            String prefKey = PREF_PREFIX+tag;
            int currentCount = prefs.getInt(prefKey, 0);
            currentCount++;
            if((currentCount==launchCount||(currentCount>=launchCount&&((currentCount-launchCount)%repeatAfter==0))&&(repeatAfter==-1||(repeatAfter>-1&&prefs.getBoolean(PREF_PREFIX+"_bool_"+tag, true))))){
                DialogFragment fragment = newInstance(message, title,okTitle,dismissTitle, showAgainText);
                    fragment.show(fm,tag);
            }
            prefs.edit().putInt(prefKey,currentCount).commit();
        }
    }

    public static void showDialog(Context context, Builder builder, int launchCount, int repeatCount, String showAgainText){



    }

    private static DialogFragment newInstance(String message, String title, String okTitle, String dismissTitle, String showAgainText){
        DialogFragment fragment = new ReminderDialogFragment();

        Bundle args = new Bundle(4);
        args.putString(EXTRA_MESSAGE, message);
        args.putString(EXTRA_TITLE, title);
        args.putString(EXTRA_OK_TITLE, okTitle);
        args.putString(EXTRA_DISMISS_TITLE, dismissTitle);
        args.putString(EXTRA_SHOW_AGAIN_TEXT, showAgainText);

        fragment.setArguments(args);

        return fragment;

    }

}
