package se.davison.autoflasher.utils;

import android.os.Handler;
import android.os.Message;

import java.lang.ref.WeakReference;


/**
 * Created by richard on 26/01/15.
 */
public class ThreadHandler extends Handler {

    public interface MessageHandler {

        void handleMessage(Message msg);
    }

    WeakReference<MessageHandler> reference;

    public ThreadHandler(MessageHandler messageHandler){
        reference = new WeakReference<MessageHandler>(messageHandler);
    }

    @Override
    public void handleMessage(Message msg) {
        MessageHandler messageHandler = reference.get();
        if (messageHandler != null) {
            messageHandler.handleMessage(msg);
        }
    }
}
