package se.davison.autoflasher.utils;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.util.Pair;

public class FileUtils {

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    private static final Comparator<File> FILE_COMPARATOR_LAST_MODIFIED = new Comparator<File>() {
        public int compare(File f1, File f2) {
            return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
        }
    };

    private static final Comparator<File> FILE_COMPARATOR_NAME = new Comparator<File>() {
        public int compare(File f1, File f2) {
            return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
        }
    };


    public static Pair<List<File>, List<File>> listFiles(final File dir, boolean useRoot, boolean orderByLastModified, final FilenameFilter directoryFilter, final FilenameFilter fileFilter) {
        String path = dir.getAbsolutePath();
        boolean listingFailed = false;

        List<String> processLines = null;
        List<File> fileList = new ArrayList<>(100);
        List<File> folderList = new ArrayList<>(100);
        try {
            processLines = ProcessExecutor.execute(useRoot, String.format("ls -lL%sp  \"%s\" | tr -s ' ' | cut -f8- -d' '",(orderByLastModified ? "t" : ""), path));
        } catch (IOException | InterruptedException e) {
            listingFailed = true;
            e.printStackTrace();
        }
        if (processLines != null) {
            for (String l : processLines) {
                if (l.isEmpty()) {
                    continue;
                }
                File f = new File(path, l);
                if (l.endsWith("/")) {
                    if (directoryFilter != null && !directoryFilter.accept(dir, l.substring(0, l.length() - 1))) {
                        continue;
                    }
                    folderList.add(f);
                } else {
                    if (fileFilter != null && !fileFilter.accept(dir, l)) {
                        continue;
                    }
                    fileList.add(new File(path, l));
                }
            }
            listingFailed = processLines.isEmpty();
        }

        //use legacy file listing if the list is empty and we got an exception
        if(listingFailed) {
            Comparator<File> comparator = orderByLastModified ? FILE_COMPARATOR_LAST_MODIFIED : FILE_COMPARATOR_NAME;
            listFilesLegacy(dir, folderList, comparator, new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return file.isDirectory() && (directoryFilter == null || directoryFilter.accept(dir, file.getName()));
                }
            });
            listFilesLegacy(dir, fileList, comparator, new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return !file.isDirectory() && (fileFilter == null || fileFilter.accept(dir, file.getName()));
                }
            });
        }
        return new Pair<>(folderList, fileList);

    }

    private static void listFilesLegacy(File dir, List<File> list, Comparator<File> comparator, FileFilter fileFilter) {
        list.clear();
        File[] filteredList = dir.listFiles(fileFilter);
        if (filteredList != null) {
            list.addAll(Arrays.asList(filteredList));
            Collections.sort(list, comparator);
        }
    }

    public static boolean exists(File file, boolean useRoot) {
        try {
            List<String> lines = ProcessExecutor.execute(true, String.format("test -e \"%s\" && echo 1 || echo 0", file.getAbsolutePath()));
            if (!lines.isEmpty()) {
                return lines.get(0).equals("1");
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    public interface Callback {
        void onProgress(float progress, long size);
    }

    private Callback callback;

    public FileUtils(Callback callback) {
        this.callback = callback;
    }

    public void copyAssets(Context context, String folder, String file, File outputDir) throws IOException {
        if (folder == null) {
            folder = "";
        }
        AssetManager assetManager = context.getAssets();
        if (file != null && !new File(outputDir, file).exists()) {
            if (!folder.isEmpty() && !folder.endsWith("/")) {
                folder += "/";
            }
            copySingleAsset(assetManager, outputDir, folder + file, file);
        } else if (file == null) {
            copyFileTree(assetManager, assetManager.list(folder), outputDir, null);
        }
    }

    private void copySingleAsset(AssetManager assetManager, File outputDir, String assetPath, String fileName) throws IOException {
        String fullOutputPath = outputDir.getAbsolutePath() + "/" + fileName;
        long size = 0;
        try {
            AssetFileDescriptor fileDescriptor = assetManager.openFd(assetPath);
            size = fileDescriptor.getLength();
        } catch (IOException ex) {
            //ignore this if fileDescriptior failes
        }

        if (!outputDir.exists()) {
            outputDir.mkdirs();
        }
        copy(size, assetManager.open(assetPath), new FileOutputStream(fullOutputPath), fullOutputPath);
    }

    private void copyFileTree(AssetManager assetManager, String[] filesNames, File outputDir, String path) throws IOException {
        for (String file : filesNames) {
            String realPath = path == null ? file : path + "/" + file;
            String[] subFiles = assetManager.list(file);
            if (subFiles.length > 0) {
                copyFileTree(assetManager, subFiles, new File(outputDir.getAbsolutePath() + "/" + file), realPath);
            } else {
                copySingleAsset(assetManager, outputDir, realPath, file);
            }
        }
    }

    private File copy(long size, InputStream in, OutputStream out, String fullOutputPath) throws IOException {

        byte[] buffer = new byte[1024];
        int read;
        long count = 0;
        int oldPercent = 0;
        while ((read = in.read(buffer)) != -1) {

            count += read;
            int percent = (int) (((float) count / (float) size) * 100);
            if (percent != oldPercent) {
                oldPercent = percent;
                if (callback != null) {
                    callback.onProgress((float) percent / 100f, size);
                }
            }

            out.write(buffer, 0, read);
        }
        in.close();
        in = null;

        // write the output file
        out.flush();
        out.close();
        out = null;
        return new File(fullOutputPath);

    }

    public File copyFile(File inputFile, File outputPath, boolean useRoot) throws IOException, InterruptedException {
        //create output directory if it doesn't exist
        if (!outputPath.exists()) {
            outputPath.mkdirs();
        }

        if (!outputPath.isDirectory()) {
            throw new IllegalStateException("outputPath is not a directory");
        }

        String fullOutputPath = outputPath.getAbsolutePath() + "/" + inputFile.getName();
        if (useRoot) {
            copyWithRoot(inputFile, fullOutputPath);
            return new File(fullOutputPath);
        }

        return copy(inputFile.length(), new FileInputStream(inputFile), new FileOutputStream(fullOutputPath), fullOutputPath);
    }

    private void copyWithRoot(File inputFile, String fullOutputPath) throws IOException, InterruptedException {
        ProcessExecutor.execute(true, String.format("cp \"%s\" \"%s\"",inputFile.getAbsolutePath(), fullOutputPath));
    }

}
