package se.davison.autoflasher.utils;

/**
 * Created by richard on 2017-02-22.
 */
public class PurchaseException extends Throwable {

    private int responseCode = -1;

    public PurchaseException(int responseCode) {
        this.responseCode = responseCode;
    }

    public int getResponseCode() {
        return responseCode;
    }
}
