package se.davison.autoflasher.utils;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;

/**
 * Created by richard on 01/12/15.
 */
public class SeparatorItemDecoration extends RecyclerView.ItemDecoration {

    private int dividerHeight;

    public static float pxToDp(int px){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float dp = ((float)px) / (metrics.densityDpi / 160f);
        return Math.round(dp);
    }

    public static int dpToPx(float dp){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        return (int) (dp * (metrics.densityDpi / 160f));
    }

    public SeparatorItemDecoration(int dpPaddingLeft, int dpPaddingRight, int color){
        separatorPaddingLeft = dpToPx(dpPaddingLeft);
        separatorPaddingRight = dpToPx(dpPaddingRight);
        dividerHeight = dpToPx(0.5f);
        separatorPaint.setColor(color);
    }

    private int separatorPaddingLeft;
    private int separatorPaddingRight;

    private Paint separatorPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDraw(c, parent, state);
        for (int i = parent.getChildCount() - 1; i >= 0; i--) {
            View child = parent.getChildAt(i);
            drawVertical(c,parent,child);
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
    }

    public void drawVertical(Canvas c, RecyclerView parent, View child) {
        final int left = parent.getPaddingLeft()+ separatorPaddingLeft;
        final int right = parent.getWidth()-separatorPaddingRight-parent.getPaddingRight();
        final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                .getLayoutParams();
        final int top = child.getBottom() + params.bottomMargin;
        final int bottom = top + dividerHeight;
        //final int bottom = top + mDivider.getIntrinsicHeight();
        c.drawRect(left, top, right, bottom, separatorPaint);
    }
}
