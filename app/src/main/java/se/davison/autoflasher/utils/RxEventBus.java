package se.davison.autoflasher.utils;

import rx.Observable;
import rx.functions.Func1;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

/**
 * Created by richard on 29/11/15.
 */
public class RxEventBus {


    private final Subject<Object, Object> _bus = new SerializedSubject<>(PublishSubject.create());

    public void send(Object o) {
        _bus.onNext(o);
    }

    public <T extends Object> Observable<T> subscribe(final Class<T> eventType) {



        return _bus.filter(new Func1<Object, Boolean>() {

            @Override
            public Boolean call(Object arg0) {
                return eventType.isInstance(arg0);
            }

        })
                .cast(eventType);
    }
}
