package se.davison.autoflasher.utils;

import android.app.backup.BackupAgentHelper;
import android.app.backup.SharedPreferencesBackupHelper;
import android.preference.PreferenceManager;

/**
 * Created by richard on 2017-02-28.
 */

public class PreferencesBackupAgent extends BackupAgentHelper {


    // A key to uniquely identify the set of backup data
    static final String PREFS_BACKUP_KEY = "prefs";

    @Override
    public void onCreate() {
        super.onCreate();

        SharedPreferencesBackupHelper helper =
                new SharedPreferencesBackupHelper(this, getApplicationContext().getPackageName() + "_preferences");
        addHelper(PREFS_BACKUP_KEY, helper);
    }
}
