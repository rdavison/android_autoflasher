package se.davison.autoflasher.utils;

/**
 * Created by richard on 17/01/15.
 */

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.os.EnvironmentCompat;
import android.text.TextUtils;
import android.util.Log;

import se.davison.autoflasher.model.FlashFile;

public class ExternalStorage {
	
	public static final int SD_CARD = 0;
	public static final int EXTERNAL_SD_CARD = 1;
	
	private static final String SD_CARD_MOUNT_PATH = "/mnt/sdcard";
	
	public static boolean onSdCard(File file) {
		return file.getAbsolutePath().startsWith(SD_CARD_MOUNT_PATH) || file.getAbsolutePath().startsWith(getSdCardPath()) || file.getAbsolutePath().startsWith("/sdcard");
	}
	
	private static final Pattern DIR_SEPORATOR = Pattern.compile("/");
	
	public static List<String> getStorageDirectories(final Context context, final boolean includePrimaryExternalStorage) {
		final File[] externalCacheDirs = ContextCompat.getExternalCacheDirs(context);
		if (externalCacheDirs == null || externalCacheDirs.length == 0) {
			return null;
		}
		if (externalCacheDirs.length == 1) {
			if (externalCacheDirs[0] == null) {
				return null;
			}
			final String storageState = EnvironmentCompat.getStorageState(externalCacheDirs[0]);
			if (!Environment.MEDIA_MOUNTED.equals(storageState)) {
				return null;
			}
			if (!includePrimaryExternalStorage && Environment.isExternalStorageEmulated()) {
				return null;
			}
		}
		final List<String> result = new ArrayList<>();
		if (includePrimaryExternalStorage || externalCacheDirs.length == 1) {
			result.add(getRootOfInnerSdCardFolder(externalCacheDirs[0]));
		}
		for (int i = 1; i < externalCacheDirs.length; ++i) {
			final File file = externalCacheDirs[i];
			if (file == null) {
				continue;
			}
			final String storageState = EnvironmentCompat.getStorageState(file);
			if (Environment.MEDIA_MOUNTED.equals(storageState)) {
				result.add(getRootOfInnerSdCardFolder(externalCacheDirs[i]));
			}
		}
		if (result.isEmpty()) {
			return null;
		}
		return result;
	}
	
	/**
	 * Given any file/folder inside an sd card, this will return the path of the sd card
	 */
	private static String getRootOfInnerSdCardFolder(File file) {
		if (file == null) {
			return null;
		}
		
		String environmentPath = Environment.getExternalStorageDirectory().getAbsolutePath();
		
		if(file.getAbsolutePath().startsWith(environmentPath)){
			return environmentPath;
		}
		
		file = file.getParentFile();
		File parentFile = file;
		
		while(file != null && file.list() != null && !file.getName().equals("storage")){
			parentFile = file;
			file = file.getParentFile();
		}
		
		return parentFile.getAbsolutePath();
	}
	
	
//	public static List<String> getStorageDirectories() {
//		boolean addedRaw = false;
//		// Final set of paths
//		final List<String> rv = new ArrayList<String>();
//		// Primary physical SD-CARD (not emulated)
//		final String rawExternalStorage = System.getenv("EXTERNAL_STORAGE");
//		// All Secondary SD-CARDs (all exclude primary) separated by ":"
//		final String rawSecondaryStoragesStr = System.getenv("SECONDARY_STORAGE");
//		// Primary emulated SD-CARD
//		final String rawEmulatedStorageTarget = System.getenv("EMULATED_STORAGE_TARGET");
//		final String environmentPath = Environment.getExternalStorageDirectory().getAbsolutePath();
//		if (TextUtils.isEmpty(rawEmulatedStorageTarget)) {
//			// Device has physical external storage; use plain paths.
//			if (TextUtils.isEmpty(rawExternalStorage)) {
//				// EXTERNAL_STORAGE undefined; falling back to default.
//				rv.add("/storage/sdcard0");
//			} else {
//				rv.add(rawExternalStorage);
//			}
//
//			File rawStorage = new File(rv.get(0));
//			addedRaw = rawStorage.canWrite() && rawStorage.listFiles().length != 0;
//
//		}
//
//		if (!addedRaw) {
//			if (!rv.isEmpty()) {
//				rv.remove(0);
//			}
//			// Device has emulated storage; external storage paths should have
//			// userId burned into them.
//			final String rawUserId;
//			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
//				rawUserId = "";
//			} else {
//
//				final String[] folders = DIR_SEPORATOR.split(environmentPath);
//				final String lastFolder = folders[folders.length - 1];
//				boolean isDigit = false;
//				try {
//					Integer.valueOf(lastFolder);
//					isDigit = true;
//				} catch (NumberFormatException ignored) {
//				}
//				rawUserId = isDigit ? lastFolder : "";
//			}
//			// /storage/emulated/0[1,2,...]
//			if (TextUtils.isEmpty(rawUserId) && rawEmulatedStorageTarget != null) {
//				rv.add(rawEmulatedStorageTarget);
//			} else if (rawEmulatedStorageTarget != null) {
//				rv.add(rawEmulatedStorageTarget + File.separator + rawUserId);
//			} else {
//				rv.add(environmentPath);
//			}
//		}
//		//check if sdcard is empty
//
//
//		// Add all secondary storages
//		if (!TextUtils.isEmpty(rawSecondaryStoragesStr)) {
//			// All Secondary SD-CARDs splited into array
//			final String[] rawSecondaryStorages = rawSecondaryStoragesStr.split(File.pathSeparator);
//			Collections.addAll(rv, rawSecondaryStorages);
//		}
//		return rv;
//	}
	
	/**
	 * @return True if the external storage is available. False otherwise.
	 */
	public static boolean isAvailable() {
		String state = Environment.getExternalStorageState();
		return Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
	}
	
	public static String getSdCardPath() {
		return Environment.getExternalStorageDirectory().getPath() + "/";
	}
	
	/**
	 * @return True if the external storage is writable. False otherwise.
	 */
	public static boolean isWritable() {
		String state = Environment.getExternalStorageState();
		return Environment.MEDIA_MOUNTED.equals(state);
		
	}
	
	public static boolean isRoot(List<String> storages, File file) {
		String path = file.getAbsolutePath();
		if (onSdCard(file)) {
			return false;
		}
		for (String storagePath : storages) {
			if (path.startsWith(storagePath)) {
				return false;
			}
		}
		return true;
	}
}
