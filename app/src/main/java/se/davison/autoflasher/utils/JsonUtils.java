package se.davison.autoflasher.utils;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import se.davison.autoflasher.model.FlashRule;

/**
 * Created by richard on 2017-03-05.
 */

public class JsonUtils {
	
	private static final Gson GSON = new Gson();
	
	
	public static <T> T loadJson(SharedPreferences prefs, String preferenceKey, TypeToken<T> typeToken) throws Exception {
		String json = prefs.getString(preferenceKey, null);
		if (json != null) {
			return GSON.fromJson(json, typeToken.getType());
		}
		try {
			return (T) typeToken.getRawType().newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
			throw e;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	public static void saveJson(SharedPreferences prefs, String preferenceKey, Object items, boolean background) {
		SharedPreferences.Editor editor = prefs.edit().putString(preferenceKey, GSON.toJson(items));
		if (background) {
			editor.apply();
		} else {
			editor.commit();
		}
	}
}
