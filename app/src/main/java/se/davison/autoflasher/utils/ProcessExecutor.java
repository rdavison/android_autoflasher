package se.davison.autoflasher.utils;

import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by richard on 2017-02-17.
 */

public class ProcessExecutor {


    @SuppressWarnings("ThrowFromFinallyBlock")
    public static List<String> execute(boolean sudo, String... commands) throws IOException, InterruptedException {
        DataOutputStream outputStream = null;
        BufferedReader bufferedReader = null;
        Process su = null;
        Exception exception = null;
        List<String> processLines = new ArrayList<>(400);
        try {
            su = Runtime.getRuntime().exec(sudo?"su":"sh");
            outputStream = new DataOutputStream(su.getOutputStream());
            bufferedReader = new BufferedReader(
                    new InputStreamReader(su.getInputStream()));

            for (String s : commands) {
                outputStream.writeBytes(s + "\n");
                outputStream.flush();
            }

            outputStream.writeBytes("exit\n");
            outputStream.flush();
            su.waitFor();

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                processLines.add(line);
            }
        } catch (IOException | InterruptedException ex) {
            exception = ex;
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
            if (su != null) {
                su.destroy();
            }
            if (bufferedReader != null) {
                bufferedReader.close();
            }

            if (exception != null) {
                if (exception instanceof IOException) {
                    throw (IOException) exception;
                } else {
                    throw (InterruptedException) exception;
                }

            }
        }
        return processLines;
    }
}
