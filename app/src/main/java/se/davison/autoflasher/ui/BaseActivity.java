package se.davison.autoflasher.ui;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;

import se.davison.autoflasher.R;
import se.davison.autoflasher.utils.Utils;

/**
 * Created by richard on 17/01/15.
 */
public class BaseActivity extends AppCompatActivity {
    private Toolbar actionBarToolbar;



    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getActionBarToolbar().post(new Runnable() {
            @Override
            public void run() {
                getActionBarToolbar().getBackground().setAlpha(255);
            }
        });
//        getActionBarToolbar().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                getActionBarToolbar().getBackground().setAlpha(255);
//                getActionBarToolbar().invalidate();
//            }
//        },5000);
    }

    protected Toolbar getActionBarToolbar() {
        if (actionBarToolbar == null) {
            actionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
            if (actionBarToolbar != null) {
                setSupportActionBar(actionBarToolbar);
            }
        }
        return actionBarToolbar;
    }
}
