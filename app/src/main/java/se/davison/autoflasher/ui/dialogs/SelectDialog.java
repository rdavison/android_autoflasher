package se.davison.autoflasher.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class SelectDialog extends BaseDialogFragment {
	
	private static final String EXTRA_ITEMS = "extra_items";
    public static final String FRAGMENT_TAG = "select_dialog_fragment_tag";

    public static class Builder extends BaseDialogFragment.Builder{

        private List<SelectItem> items;

        public Builder(DialogCallback callback, SelectItem... items) {
            super(callback);
            this.items = Arrays.asList(items);
        }

        public Builder(DialogCallback callback, List<SelectItem> items) {
            super(callback);
            this.items = items;
        }


        @Override
        public SelectDialog show() {
            String dialogTag = tag==null?FRAGMENT_TAG:tag;
            SelectDialog dialog = SelectDialog.newInstance(dialogTag, message,title,cancelable,items);
			if(fragment!=null){
				dialog.setTargetFragment(fragment,0);
			}
            dialog.show(fm);
			return dialog;
        }
    }
	
	public static class SelectDialogEvent extends DialogEvent{
		
		public static final int CANCELED = -1;
		public SelectDialogEvent(String tag, int result) {
			super(tag, result);
		}
	}

	public static class SelectItem implements Serializable {
		int key;
		String title;
		int iconResource;
		public SelectItem(int key, String title, int iconResource) {
			this.key = key;
			this.title = title;
			this.iconResource = iconResource;
		}
	}
	
	private static class SelectItemAdater extends ArrayAdapter<SelectItem>{
		
		public SelectItemAdater(Context context,
				List<SelectItem> items) {
			super(context, 0, items);
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			Pair<TextView, ImageView> viewHolder;
			
			if(convertView==null){
				
				Context context = getContext();
				
				float density = context.getResources().getDisplayMetrics().density;
				
				LinearLayout ll = new LinearLayout(context);
				ll.setOrientation(LinearLayout.HORIZONTAL);
				ll.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, (int) (48*density)));
				ll.setPadding((int)(8*density), 0,(int)( 8*density), 0);
				
				TextView txtTitle = new TextView(context);
				txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
				ImageView imgIcon = new ImageView(context);
				
				txtTitle.setGravity(Gravity.CENTER_VERTICAL);
				
				LinearLayout.LayoutParams tvLp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
				LinearLayout.LayoutParams ivLp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
				
				ivLp.gravity = Gravity.CENTER_VERTICAL;
				ivLp.setMargins(0, 0, (int)(12*density), 0);
				
				ll.addView(imgIcon, ivLp);
				ll.addView(txtTitle, tvLp);
				
				convertView = ll;
				
				viewHolder = new Pair<TextView, ImageView>(txtTitle, imgIcon);
				
				convertView.setTag(viewHolder);
				
			}else{
				viewHolder = (Pair<TextView, ImageView>) convertView.getTag();
			}
			
			SelectItem item = getItem(position);
			
			viewHolder.first.setText(item.title);
			viewHolder.second.setImageResource(item.iconResource);
			
			return convertView;
		}
	}
	

	public static SelectDialog newInstance(String tag, String message, String title, boolean cancelable,List<SelectItem> items) {

		SelectDialog dialogFragment = new SelectDialog();
		Bundle args = new Bundle();
		args.putSerializable(EXTRA_ITEMS, (Serializable) items);
		dialogFragment.setArguments(tag, message, title, cancelable, args);
		return dialogFragment;

	}

	@SuppressWarnings("unchecked")
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		final List<SelectItem> items = (List<SelectItem>) getArguments().getSerializable(EXTRA_ITEMS);
		
		SelectItemAdater adapter = new SelectItemAdater(getActivity(), items);

        AlertDialog.Builder builder = getDefaultBuilder();
        return builder.setAdapter(adapter, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                postEvent(new SelectDialogEvent(getFragmentTag(), which));
            }
        }).create();
	}

}
