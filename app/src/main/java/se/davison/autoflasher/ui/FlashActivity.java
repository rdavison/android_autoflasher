package se.davison.autoflasher.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.io.Serializable;
import java.util.List;

import se.davison.autoflasher.R;
import se.davison.autoflasher.model.FlashRule;
import se.davison.autoflasher.utils.Recovery;

/**
 * Created by richard on 20/01/15.
 */
public class FlashActivity extends BaseActivity{

    private static final String EXTRA_RULES = "extra_rules";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fragment_wrapper);


        List<FlashRule> rules = (List<FlashRule>) getIntent().getExtras().getSerializable(EXTRA_RULES);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean twrp = prefs.getInt(MainFragment.PREF_KEY_RECOVERY,Recovery.TWRP) == Recovery.TWRP;

        Toolbar toolbar = getActionBarToolbar();
        getSupportActionBar().setTitle(getString(R.string.flash_confirm_title));
        getSupportActionBar().setSubtitle(getString(R.string.select_files_to_flash, getResources().getQuantityString(R.plurals.files,
                twrp?2:1)));
        //toolbar.setTitle();
        toolbar.setNavigationIcon(R.drawable.ic_toolbar_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                /*Intent upIntent = NavUtils.getParentActivityIntent(FlashActivity.this);
                if (NavUtils.shouldUpRecreateTask(FlashActivity.this, upIntent)||  getIntent().getAction() != null) {
                    TaskStackBuilder.create(FlashActivity.this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    NavUtils.navigateUpTo(FlashActivity.this, upIntent);
                }*/
            }
        });

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.cnt_fragment, FlashFragment.newInstance(rules))
                    .commit();
        }
    }

    public static void startActivity(Context context, List<FlashRule> rules) {
        Intent intent = new Intent(context, FlashActivity.class);
        intent.putExtra(EXTRA_RULES, (Serializable) rules);
        context.startActivity(intent);
    }
}
