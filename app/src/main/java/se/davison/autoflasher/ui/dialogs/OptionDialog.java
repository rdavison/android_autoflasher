package se.davison.autoflasher.ui.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import se.davison.autoflasher.R;

/**
 * Created by richard on 2017-03-03.
 */

public class OptionDialog extends BaseDialogFragment {

	private static final String EXTRA_ITEMS = "extra_items";
	public static final String FRAGMENT_TAG = "option_dialog_fragment_tag";
	private List<OptionItem> items;

	public static class Builder extends BaseDialogFragment.Builder{

		private List<OptionItem> items;

		public Builder(DialogCallback callback, OptionItem... items) {
			super(callback);
			this.items = Arrays.asList(items);
		}

		public Builder(DialogCallback callback, List<OptionItem> items) {
			super(callback);
			this.items = items;
		}


		@Override
		public OptionDialog show() {
			String dialogTag = tag==null?FRAGMENT_TAG:tag;
			OptionDialog dialog = OptionDialog.newInstance(dialogTag, message,title,cancelable,items);
			if(fragment!=null){
				dialog.setTargetFragment(fragment,0);
			}
			dialog.show(fm);
			return dialog;
		}
	}

	public static class OptionDialogEvent extends DialogEvent{

		public List<OptionItem> items;

		public OptionDialogEvent(String tag, List<OptionItem> items) {
			super(tag, 0);
			this.items = items;
		}
	}

	public static class OptionItem implements Serializable {
		public String title;
		public boolean selected;

		public OptionItem(String title, boolean selected) {
			this.title = title;
			this.selected = selected;
		}
	}

	public static OptionDialog newInstance(String tag, String message, String title, boolean cancelable,List<OptionItem> items) {

		OptionDialog dialogFragment = new OptionDialog();
		Bundle args = new Bundle();
		args.putSerializable(EXTRA_ITEMS, (Serializable) items);
		dialogFragment.setArguments(tag, message, title, cancelable, args);
		return dialogFragment;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		items = (List<OptionItem>) getArguments().getSerializable(EXTRA_ITEMS);

	}

	@SuppressWarnings("unchecked")
	public Dialog onCreateDialog(Bundle savedInstanceState) {





		String[] titles = new String[items.size()];
		boolean[] checkedItems = new boolean[items.size()];
		for(int i = 0; i < items.size(); i++){
			OptionItem item = items.get(i);
			titles[i] = item.title;
			checkedItems[i] = item.selected;
		}

		android.support.v7.app.AlertDialog.Builder builder = getDefaultBuilder();

		return builder.setMultiChoiceItems(titles, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which, boolean isChecked) {
				items.get(which).selected = isChecked;
			}
		})
				.setNegativeButton(R.string.cancel,this)
				.setPositiveButton(R.string.save, this).create();
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		if(which == DialogInterface.BUTTON_POSITIVE){
			postEvent(new OptionDialogEvent(getFragmentTag(), items));
		}
	}
}
