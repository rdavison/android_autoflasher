package se.davison.autoflasher.ui;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v4.util.SparseArrayCompat;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.google.gson.reflect.TypeToken;
import com.stericson.RootShell.exceptions.RootDeniedException;
import com.stericson.RootTools.RootTools;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import se.davison.autoflasher.R;
import se.davison.autoflasher.adapters.CheckBoxAdapter;
import se.davison.autoflasher.model.FlashFile;
import se.davison.autoflasher.model.FlashRule;
import se.davison.autoflasher.ui.dialogs.BaseDialogFragment;
import se.davison.autoflasher.ui.dialogs.ErrorDialog;
import se.davison.autoflasher.ui.dialogs.OptionDialog;
import se.davison.autoflasher.ui.dialogs.ProgressDialog;
import se.davison.autoflasher.utils.ExternalStorage;
import se.davison.autoflasher.utils.FileUtils;
import se.davison.autoflasher.utils.JsonUtils;
import se.davison.autoflasher.utils.ProcessExecutor;
import se.davison.autoflasher.utils.Recovery;
import se.davison.autoflasher.utils.ResourceUtils;
import se.davison.autoflasher.utils.Utils;
import se.davison.autoflasher.widget.SwipeToDoView;


/**
 * Created by richard on 20/01/15.
 */
public class FlashFragment extends RootFragment {

    private static final String EXTRA_RULES = "extra_rules";
    private static final String TAG = FlashFragment.class.getSimpleName();

    private static final String PREF_KEY_WIPE_CACHE = "pref_key_wipe_cache";
    private static final String PREF_KEY_WIPE_DATA = "pref_key_wipe_data";
    private static final String PREF_KEY_BACKUP = "pref_key_backup";
    private static final String PREF_KEY_ROOT_ALLOWED = "pref_key_root_allowed";
    private static final String PREF_KEY_FIX_PERMISSIONS = "pref_key_fix_permissions";
    private static final String PREF_KEY_REMOVE_OLD_FILES = "pref_key_remove_old_files";
    private static final String PREF_KEY_WIPE_SYSTEM = "pref_key_wipe_system";
    private static final String PREF_KEY_BACKUP_OPTIONS = "pref_key_backup_options";
    private static final String PREF_KEY_FORMAT_SYSTEM_F2FS = "pref_key_format_system_f2fs";
    private static final String DIALOG_TAG_FLASH = "dialog_tag_flash";
    private static final String DIALOG_TAG_BACKUP_OPTIONS = "dialog_tag_backup_options";
    
    private static final String WIPE_SYSTEM_ZIP = "wipe_system.zip";

    public static final int MESSAGE_SET_PROGRESSBAR_INDETERMINATE = 0;
    public static final int MAX_FLASH_WAIT_TIME = 60;
    private List<FlashRule> rules;
    
    
    
    private CheckBoxAdapter<FlashFile> adapter;
    private SharedPreferences prefs;
    private ArrayList<FlashFile> filesToFlash;
    private ArrayList<File> filesToRemove;
    private boolean threadRunning = false;
    private boolean twrp;
    private Recovery recovery;
    private SwipeToDoView swipeToDoView;
    private CompositeSubscription compositeSubscription;
    private List<String> storages;
    private TextView txtEmpty;
    private ListView listView;
    private LinkedHashMap<String,String> backupOptionNames;
    private List<OptionDialog.OptionItem> backupOptionItems = new ArrayList<>();
    private HashMap<String, Boolean> backupOptionMap;
    
    @Override
    public void onDestroy() {
        if(compositeSubscription!=null){
            compositeSubscription.unsubscribe();
        }
        super.onDestroy();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        storages = ExternalStorage.getStorageDirectories(getContext(), true);

        rules = (List<FlashRule>) getArguments().getSerializable(EXTRA_RULES);
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        twrp = prefs.getInt(MainFragment.PREF_KEY_RECOVERY, 0) == 0;

        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        backupOptionNames = (LinkedHashMap<String, String>) ResourceUtils.getMapResource(getContext(),R.xml.backup_options);


        recovery = new Recovery(twrp ? Recovery.TWRP : Recovery.CWM);

        setRetainInstance(true);

    }

    @Override
    public void onResume() {
        super.onResume();
        showRequireRootDialog();
        filesToFlash = new ArrayList<FlashFile>();
        filesToRemove = new ArrayList<File>();
        for (final FlashRule rule : rules) {
            File dir = rule.getFile().getParentFile();

            List<File> filteredList = FileUtils.listFiles(dir, true, true, null, rule).second;
            if (!filteredList.isEmpty()) {
                filesToFlash.add(new FlashFile(filteredList.remove(0)));
                filesToRemove.addAll(filteredList);
            }
        }

        long flashFileSize = 0;
        for (FlashFile file : filesToFlash) {
            flashFileSize += file.length();
        }

        boolean hasSpace = RootTools.hasEnoughSpaceOnSdCard(flashFileSize);

        if (!filesToFlash.isEmpty() && hasSpace) {
            adapter = new CheckBoxAdapter<FlashFile>(getActivity(), listView, twrp ? R.layout.list_item_file_checkbox : R.layout.list_item_file_radio, filesToFlash, twrp);
            adapter.setChoiceMode(twrp ? ListView.CHOICE_MODE_MULTIPLE : ListView.CHOICE_MODE_SINGLE);
            adapter.setChecked(0, true);
            listView.setAdapter(adapter);
            swipeToDoView.setEnabled(true);
            listView.setItemsCanFocus(false);

        } else {
            listView.setVisibility(View.GONE);
            swipeToDoView.setEnabled(false);
            txtEmpty.setVisibility(View.VISIBLE);

            txtEmpty.setText(Html.fromHtml(getString(hasSpace ? R.string.no_files : R.string.err_size).replace("{EMOJI}", new String(Character.toChars(0x1f622))), new Html.ImageGetter() {
                @Override
                public Drawable getDrawable(String source) {
                    Drawable drawFromPath;
                    Resources res = getResources();
                    int path =
                            res.getIdentifier(source, "drawable",
                                    getActivity().getApplicationContext().getPackageName());
                    drawFromPath = ContextCompat.getDrawable(getContext(), path);
                    drawFromPath.setBounds(0, 0, drawFromPath.getIntrinsicWidth(),
                            drawFromPath.getIntrinsicHeight());
                    return drawFromPath;
                }
            }, null));
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_flash, container, false);

        Utils.setupAds((AdView) view.findViewById(R.id.ad_view), Utils.isPremium(prefs, getActivity()));

        txtEmpty = (TextView) view.findViewById(R.id.txt_list_empty);
        listView = (ListView) view.findViewById(R.id.lst_rules);

        swipeToDoView = (SwipeToDoView) view.findViewById(R.id.swipe_to_flash);


        swipeToDoView.setTextColor(Color.WHITE);
        swipeToDoView.setOnSwipedListener(new SwipeToDoView.OnSwipedListener() {
            @Override
            public void onSwiped() {
                prepareFlash();
            }
        });

        final CheckBox chkBackup = (CheckBox) view.findViewById(R.id.chk_backup);
        CheckBox chkRemoveOld = (CheckBox) view.findViewById(R.id.chk_remove_old);
        CheckBox chkWipeCache = (CheckBox) view.findViewById(R.id.chk_wipe_cache);
        CheckBox chkWipeData = (CheckBox) view.findViewById(R.id.chk_wipe_data);
        CheckBox chkWipeSystem = (CheckBox) view.findViewById(R.id.chk_wipe_system);
        final Button btnBackupOptions = (Button)view.findViewById(R.id.btn_backup_options);


        loadBackupOptions();

        
        btnBackupOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new OptionDialog.Builder(FlashFragment.this, backupOptionItems).tag(DIALOG_TAG_BACKUP_OPTIONS).title(getString(R.string.backup_options)).show();
            }
        });

        List<Pair<String, CheckBox>> settingsBoxes = new ArrayList<Pair<String, CheckBox>>(4);
        settingsBoxes.add(new Pair<String, CheckBox>(PREF_KEY_WIPE_CACHE, chkWipeCache));
        settingsBoxes.add(new Pair<String, CheckBox>(PREF_KEY_BACKUP, chkBackup));
        settingsBoxes.add(new Pair<String, CheckBox>(PREF_KEY_REMOVE_OLD_FILES, chkRemoveOld));
        settingsBoxes.add(new Pair<String, CheckBox>(PREF_KEY_WIPE_DATA, chkWipeData));
        if (twrp) {
            settingsBoxes.add(new Pair<String, CheckBox>(PREF_KEY_WIPE_SYSTEM, chkWipeSystem));
        } else {
            chkWipeSystem.setVisibility(View.GONE);
        }

        for (final Pair<String, CheckBox> setting : settingsBoxes) {
            boolean isChecked = prefs.getBoolean(setting.first, false);
            setting.second.setChecked(isChecked);
            if(setting.second.equals(chkBackup)){
                btnBackupOptions.setEnabled(isChecked);
            }
            setting.second.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(buttonView.equals(chkBackup)){
                        btnBackupOptions.setEnabled(isChecked);
                    }
                    prefs.edit().putBoolean(setting.first, isChecked).commit();
                }
            });
        }


        return view;
    }
    
    private void loadBackupOptions() {
        try {
            backupOptionMap = JsonUtils.loadJson(prefs, PREF_KEY_BACKUP_OPTIONS, new TypeToken<HashMap<String, Boolean>>() {});
        } catch (Exception e) {
            backupOptionMap = new HashMap<>(backupOptionNames.size());
        }
    
        for (String optionKey : backupOptionNames.keySet()){
            String optionName = backupOptionNames.get(optionKey);
            boolean defaultValue = true;
        
            switch (optionKey.charAt(0)) {
                case '1':
                case '2':
                case '3':
                case 'E':
                case 'M':
                    defaultValue = false;
                    break;
            }
            boolean value = defaultValue;
            if (backupOptionMap.containsKey(optionKey)){
                value = backupOptionMap.get(optionKey);
            }
        
            backupOptionItems.add(new OptionDialog.OptionItem(optionName, value));
        }
    }

    private void saveBackupOptions(){
        backupOptionMap.clear();
    
        int i = 0;
        String[] keys = backupOptionNames.keySet().toArray(new String[backupOptionItems.size()]);
        for(OptionDialog.OptionItem item : backupOptionItems){
            backupOptionMap.put(keys[i], item.selected);
            i++;
        }
        JsonUtils.saveJson(prefs,PREF_KEY_BACKUP_OPTIONS,backupOptionMap, false);
    }

    public static class ProgressBarMessage {
        private final Integer progress;
        public String message;
        public boolean indeterminate;

        public ProgressBarMessage(String message, boolean indeterminate) {
            this.message = message;
            this.indeterminate = indeterminate;
            this.progress = null;
        }

        public ProgressBarMessage(int progress) {
            this.progress = progress;
        }
    }


    private void prepareFlash() {


        if (!threadRunning) {
            threadRunning = true;

            List<FlashFile> selectedFiles = adapter.getItemsWithValue(true);

            final int fileCount = selectedFiles.size();
            final SparseArrayCompat<File> finalFlashList = new SparseArrayCompat<File>(fileCount);
            final SparseArrayCompat<File> moveList = new SparseArrayCompat<File>(fileCount);

            long tempMoveFileSize = 0;

            int i = 0;
            for (FlashFile file : selectedFiles) {
                if (ExternalStorage.onSdCard(file) || file.getAbsolutePath().startsWith("/data/")) {
                    
//                    if(file.getAbsolutePath().startsWith()){
//                        file = new File()
//                    }
                    finalFlashList.append(i, file);
                } else {
                    moveList.append(i,file);
                    tempMoveFileSize += file.length();
                }
                i++;
            }

            final long moveFileSize = tempMoveFileSize;

            ProgressDialog.Builder builder = new ProgressDialog.Builder(this);
            if (moveFileSize > 0) {
                builder.max((int) (tempMoveFileSize / 1000f));
            }
            builder.cancelText(getString(R.string.cancel)).cancelable(true).message(getString(R.string.preparing_flash)).tag(DIALOG_TAG_FLASH).show();

            compositeSubscription = new CompositeSubscription();
            compositeSubscription.add(Observable.create(new Observable.OnSubscribe<ProgressBarMessage>() {


                private long processedSize = 0;

                @Override
                public void call(final Subscriber<? super ProgressBarMessage> subscriber) {
                    try {
                        FileUtils fileUtils = new FileUtils(new FileUtils.Callback() {
                            @Override
                            public void onProgress(float progress, long size) {
                                float realProgress = (processedSize + (progress * size)) / moveFileSize;
                                if (progress == 1) {
                                    processedSize += size;
                                }

                                Log.d(TAG, "realProgress+" + realProgress);

                                subscriber.onNext(new ProgressBarMessage((int) (realProgress * 100f)));

                            }
                        });
                        int count = 0;
                        File flashFolder = new File(Environment.getExternalStorageDirectory() + "/autoflasher-temp/");
                        Recovery.deleteDirectory(flashFolder);
                        if (!flashFolder.mkdirs()) {
                            subscriber.onError(new IOException("Failed to create temp directory"));
                            return;
                        }

                        int moveCount = moveList.size();


                        for(int i = 0; i < moveList.size(); i++) {
                            int key = moveList.keyAt(i);
                            File file = moveList.get(key);
                            count++;
                            String message = String.format("%s %d %s %d\n%s", getString(R.string.moving_file), count, getString(R.string.of), moveCount, file);
                            if (subscriber.isUnsubscribed()) {
                                subscriber.onCompleted();
                                return;
                            }
                            subscriber.onNext(new ProgressBarMessage(message, false));
                            finalFlashList.append(key,fileUtils.copyFile(file, flashFolder, ExternalStorage.isRoot(storages, file)));
                            File md5File = new File(file.getPath() + ".md5");
                            if(FileUtils.exists(md5File, true)){
                                fileUtils.copyFile(md5File, flashFolder,ExternalStorage.isRoot(storages, file));
                            }
                        }



                        if (prefs.getBoolean(PREF_KEY_REMOVE_OLD_FILES, false)) {
                            subscriber.onNext(new ProgressBarMessage(getString(R.string.removeing_old_files), false));
                            for (File f : filesToRemove) {
                                File md5File = new File(f.getPath() + ".md5");
                                rootDelete(md5File);
                                rootDelete(f);
                            }
                        }

                        boolean assetsCopied = false;

                        File eraseImageBinaryPath = new File(Environment.getExternalStorageDirectory() + "/tmp/");
                        new File(eraseImageBinaryPath, WIPE_SYSTEM_ZIP).delete();
                        fileUtils.setCallback(null);
                        try {
                            fileUtils.copyAssets(getActivity(), null, WIPE_SYSTEM_ZIP, eraseImageBinaryPath);
                            assetsCopied = true;
                        } catch (IOException e2) {
                            //not crucial, ignore if this fails
                            e2.printStackTrace();
                        }

                        if (subscriber.isUnsubscribed()) {
                            subscriber.onCompleted();
                            return;
                        }

                        subscriber.onNext(new ProgressBarMessage(getString(R.string.writing_recovery_script), true));


                        File[] flashFiles = new File[finalFlashList.size()];
                        for(int i = 0; i < finalFlashList.size(); i++){
                            flashFiles[i] = finalFlashList.valueAt(i);
                        }
                        
                        recovery.setFormatSystemF2fs(prefs.getBoolean(PREF_KEY_FORMAT_SYSTEM_F2FS, false));
                        recovery.files(flashFiles);
                        recovery.setBackup(prefs.getBoolean(PREF_KEY_BACKUP, false), backupOptionMap);
                        recovery.setTempDirectory(flashFolder);
                        recovery.setWipeData(prefs.getBoolean(PREF_KEY_WIPE_DATA, false));
                        recovery.setWipeCache(prefs.getBoolean(PREF_KEY_WIPE_CACHE, false));
                        recovery.setWipeSystem(prefs.getBoolean(PREF_KEY_WIPE_SYSTEM, false));
                        if (assetsCopied) {
                            recovery.setEraseImageBinary(new File(eraseImageBinaryPath, WIPE_SYSTEM_ZIP));
                        }

                        if (subscriber.isUnsubscribed()) {
                            subscriber.onCompleted();
                            return;
                        }

                        subscriber.onNext(new ProgressBarMessage(getString(R.string.rebooting, 5), true));
                        sleep(1000);
                        subscriber.onNext(new ProgressBarMessage(getString(R.string.rebooting, 4), true));
                        sleep(1000);
                        subscriber.onNext(new ProgressBarMessage(getString(R.string.rebooting, 3), true));
                        sleep(1000);
                        subscriber.onNext(new ProgressBarMessage(getString(R.string.rebooting, 2), true));
                        sleep(1000);
                        subscriber.onNext(new ProgressBarMessage(getString(R.string.rebooting, 1), true));
                        sleep(1000);
                        subscriber.onNext(new ProgressBarMessage(getString(R.string.rebooting_now), true));
                        if (subscriber.isUnsubscribed()) {
                            subscriber.onCompleted();
                            return;
                        }
                        recovery.flash();
                        sleep(5000);
                        for (int i = 0; i < MAX_FLASH_WAIT_TIME; i++) {
                            subscriber.onNext(new ProgressBarMessage(getString(R.string.flash_warning, i, MAX_FLASH_WAIT_TIME), true));
                            sleep(1000);
                        }
                        subscriber.onError(new TimeoutException());

                        subscriber.onCompleted();
                    } catch (IOException | RootDeniedException | TimeoutException | InterruptedException e) {
                        subscriber.onError(e);
                    }

                }
            }).subscribeOn(Schedulers.io()).onBackpressureBuffer().timeout(45, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<ProgressBarMessage>() {
                        @Override
                        public void call(ProgressBarMessage progressBarMessage) {
                            ProgressDialog fragment = (ProgressDialog) ProgressDialog.getInstance(getFragmentManager(), DIALOG_TAG_FLASH);
                            if (fragment != null) {
                                android.app.ProgressDialog dialog = (android.app.ProgressDialog) fragment.getDialog();
                                dialog.setMax(100);
                                if (progressBarMessage.progress != null) {
                                    Log.d(TAG, "progress in progressbar==" + progressBarMessage.progress);
                                    dialog.setProgress(progressBarMessage.progress);
                                }
                                if (progressBarMessage.message != null) {
                                    fragment.setMessage(progressBarMessage.message);
                                }
                                dialog.setIndeterminate(progressBarMessage.indeterminate);
                            }
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            Log.d(TAG, throwable.toString());

                            swipeToDoView.reset();

                            threadRunning = false;
                            ProgressDialog.dismiss(getFragmentManager(), DIALOG_TAG_FLASH);

                            throwable.printStackTrace();

                            String message = null;

                            if (throwable instanceof IOException) {
                                message = getString(R.string.err_copy_files);
                            } else if (throwable instanceof TimeoutException) {
                                message = getString(R.string.err_flash_timeout);
                            } else if (throwable instanceof InterruptedException) {
                                message = getString(R.string.err_canceled);
                            } else {
                                message = getString(R.string.err_flash) + "\n" + throwable.getMessage();
                            }

                            cleanRecovery();


                            new ErrorDialog.Builder(FlashFragment.this, ErrorDialog.ALERT).message(message).show();
                        }
                    }));
        }
    }
    
    private void cleanRecovery() {
        try {
            recovery.clean();
        } catch (TimeoutException | RootDeniedException | IOException e) {
            e.printStackTrace();
        }
    }

    private void sleep(int milliseconds) throws InterruptedException {
        Thread.sleep(milliseconds);
    }

    private void rootDelete(File file) {
        try {
            boolean result = file.delete();
            if (!result) {
                throw new SecurityException();
            }
        } catch (SecurityException ex) {
            try {
                ProcessExecutor.execute(true, String.format("rm -f \"%s\"",file.getAbsolutePath()));
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onRootAccess(int status) {
        if (status == RootFragment.DENIED || status == RootFragment.NOT_AVAILABLE) {
            getActivity().finish();
        }
    }

    public static Fragment newInstance(List<FlashRule> rules) {

        FlashFragment flashFragment = new FlashFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_RULES, (Serializable) rules);
        flashFragment.setArguments(args);
        return flashFragment;
    }

    @Override
    public void onDialogCallback(BaseDialogFragment.DialogEvent event) {
        super.onDialogCallback(event);
        if (event instanceof BaseDialogFragment.DismissDialogEvent && event.tag.equals(DIALOG_TAG_FLASH)) {

            threadRunning = false;
            if(compositeSubscription!=null) {
                compositeSubscription.unsubscribe();
            }
            cleanRecovery();
            swipeToDoView.reset();
            Toast.makeText(getContext(), getString(R.string.err_canceled), Toast.LENGTH_LONG).show();
        }
        if(event instanceof OptionDialog.OptionDialogEvent && event.tag.equals(DIALOG_TAG_BACKUP_OPTIONS)){
            backupOptionItems = ((OptionDialog.OptionDialogEvent)event).items;
            saveBackupOptions();
        }
    }
}
