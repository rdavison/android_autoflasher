package se.davison.autoflasher.ui.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;


public class ProgressDialog extends BaseDialogFragment {

    public static final String FRAGMENT_TAG = "progress_dialog_fragment_tag";
    private static final String EXTRA_CANCEL_TEXT = "extra_cancel_text";
    private static final String EXTRA_MAX_VALUE = "extra_max";

    private android.app.ProgressDialog progressDialog;
    private Integer max;


    public static class ProgressEvent {
        public float progress;
        public String tag;

        public ProgressEvent(float progress){
            this.progress = progress;
        }
    }

    public static class CanceledEvent extends DialogEvent {

        public CanceledEvent(String tag, int result) {
            super(tag, result);
        }
    }

    public static class Builder extends BaseDialogFragment.Builder {

        private String cancelText;
        private Integer max;


        public Builder(DialogCallback callback) {
            super(callback);
            cancelable = false;
        }


        public Builder cancelText(String cancelText) {
            this.cancelText = cancelText;
            return this;
        }

        public Builder max(Integer max) {
            this.max = max;
            return this;
        }

        public ProgressDialog show() {
            String dialogTag = tag == null ? FRAGMENT_TAG : tag;
            ProgressDialog dialog = ProgressDialog.newInstance(dialogTag, message, title, cancelable, cancelText, max);
            if(fragment!=null){
                dialog.setTargetFragment(fragment,0);
            }
            dialog.show(fm);
            return dialog;
        }
    }


    public static ProgressDialog newInstance(String tag, String message, String title, boolean cancelable, String cancelText, Integer max) {
        ProgressDialog dialogFragment = new ProgressDialog();
        Bundle args = new Bundle();
        args.putString(EXTRA_CANCEL_TEXT, cancelText);
        args.putBoolean(EXTRA_CANCELABLE, cancelable);
        if (max == null) {
            max = -1;
        }
        args.putInt(EXTRA_MAX_VALUE, max);
        dialogFragment.setArguments(tag, message, title, cancelable,args);
        return dialogFragment;
    }

    public void setProgress(float progressPercent){
        if (progressDialog != null && max != null) {
            int progress = (int) ((float) max * progressPercent);
            progressDialog.setProgress(progress);
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle args = getArguments();
        String title = args.getString(EXTRA_TITLE);
        String message = args.getString(EXTRA_MESSAGE);
        String cancelText = args.getString(EXTRA_CANCEL_TEXT);

        max = args.getInt(EXTRA_MAX_VALUE);
        if (max == -1) {
            max = null;
        }

        progressDialog = new android.app.ProgressDialog(getActivity());
        if (title != null) {
            progressDialog.setTitle(title);
        }
        progressDialog.setMessage(message);
        if (max != null) {
            progressDialog.setProgress(0);
            progressDialog.setMax(max);
            progressDialog.setProgressStyle(android.app.ProgressDialog.STYLE_HORIZONTAL);
        } else {
            progressDialog.setIndeterminate(true);
        }
        if (cancelText != null) {
            progressDialog.setOnCancelListener(new OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {
                    postEvent(new CanceledEvent(getFragmentTag(),0));

                }
            });
            progressDialog.setButton(android.app.ProgressDialog.BUTTON_NEGATIVE, cancelText, new OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    postEvent(new CanceledEvent(getFragmentTag(),0));
                }
            });
        }
        return progressDialog;
    }

}
