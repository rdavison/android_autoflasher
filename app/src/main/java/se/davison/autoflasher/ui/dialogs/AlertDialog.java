package se.davison.autoflasher.ui.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created by admin on 2015-08-28.
 */
public class AlertDialog extends BaseDialogFragment{

    public static final String FRAGMENT_TAG = "confirm_dialog_fragment_tag";


    public static class Builder extends BaseDialogFragment.Builder{

        public Builder(DialogCallback callback) {
            super(callback);
        }


        @Override
        public DialogFragment show() {
            String dialogTag = tag == null ? FRAGMENT_TAG : tag;
            AlertDialog dialog = AlertDialog.newInstance(dialogTag,message, title,cancelable);
            if(fragment!=null){
                dialog.setTargetFragment(fragment,0);
            }
            dialog.show(fm);
            return dialog;
        }
    }

    private static AlertDialog newInstance(String tag, String message, String title, boolean cancelable) {
        AlertDialog dialogFragment = new AlertDialog();
        Bundle args = new Bundle();
        dialogFragment.setArguments(tag, message, title, cancelable,args);
        return dialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        android.support.v7.app.AlertDialog.Builder builder = getDefaultBuilder();
        builder.setPositiveButton("OK", null);
        return builder.create();
    }
}
