package se.davison.autoflasher.ui.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

public class BaseDialogFragment extends AppCompatDialogFragment implements Dialog.OnClickListener {

    private static final String EXTRA_TAG = "__extra_dialog_tag";
    protected static final String EXTRA_MESSAGE = "__extra_message";
    protected static final String EXTRA_TITLE = "__extra_title";
    protected static final String EXTRA_CANCELABLE = "__extra_cancelable";
    private boolean called = false;
    private String tag;

    @Override
    public void onClick(DialogInterface dialog, int which) {

    }

    public interface DialogCallback{
        void onDialogCallback(DialogEvent event);
    }


    public static abstract class Builder {

        protected FragmentManager fm;
        protected String message;
        protected String title;
        protected Context context;
        protected String tag;
        protected Fragment fragment;
        protected boolean cancelable = true;



        public Builder(DialogCallback callback) {


            if(callback instanceof Fragment){
                this.fragment = (Fragment)callback;
                this.context = fragment.getActivity();
                this.fm = ((FragmentActivity)context).getSupportFragmentManager();
            }else if(callback instanceof FragmentActivity){
                this.context = (FragmentActivity)callback;
                this.fm = ((FragmentActivity)callback).getSupportFragmentManager();
            }else {
                throw new IllegalArgumentException("Callback must be instanced with a Fragment, or a FragmentActivity");
            }
        }

        public Builder cancelable(boolean cancelable) {
            this.cancelable = cancelable;
            return this;
        }

        public Builder tag(String tag){
            this.tag = tag;
            return this;
        }


        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public abstract DialogFragment show();

    }

    protected void postEvent(DialogEvent dialogEvent) {
        Fragment fragment = getTargetFragment();
        if(fragment==null){
            ((DialogCallback)getActivity()).onDialogCallback(dialogEvent);
        }else{
            ((DialogCallback)fragment).onDialogCallback(dialogEvent);
        }
    }

    public static class DialogEvent {
        public int result;
        public String tag;

        public DialogEvent(String tag, int result) {
            this.result = result;
            this.tag = tag;
        }
    }

    public class DismissDialogEvent extends DialogEvent {

        public static final int DISMISSED = 0;
        public static final int CANCELED = 1;

        public DismissDialogEvent(String tag, boolean dismissed) {
            super(tag, dismissed?DISMISSED:CANCELED);
        }
    }
    public void setArguments(String tag, String message, String title, boolean cancelable,Bundle bundle) {
        bundle.putString(EXTRA_TAG, tag);
        bundle.putString(EXTRA_MESSAGE, message);
        bundle.putString(EXTRA_TITLE, title);
        bundle.putBoolean(EXTRA_CANCELABLE, cancelable);
        called = true;
        this.tag = tag;
        setArguments(bundle);
    }

    public void setMessage(String message) {
        getArguments().putString(EXTRA_MESSAGE, message);
        Dialog dialog = getDialog();
        if(dialog instanceof ProgressDialog){
            ((ProgressDialog)dialog).setMessage(message);
        }else if(dialog instanceof AlertDialog){
            ((AlertDialog)dialog).setMessage(message);
        }
    }

    public void setTitle(String title) {
        getArguments().putString(EXTRA_TITLE, title);
        getDialog().setTitle(title);
    }


    public String getTitle() {
        return getArguments().getString(EXTRA_TITLE);
    }

    public String getMessage() {
        return getArguments().getString(EXTRA_MESSAGE);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tag = getArguments().getString(EXTRA_TAG);

        if (!called) {
            //throw new IllegalArgumentException(this.getClass().getSimpleName() + " must implement 'newInstance' pattern and call 'setArguments(tag, message, bundle)' from within that method!");
        }

        setRetainInstance(true);
    }

    public String getFragmentTag() {
        return tag;
    }

    public void show(FragmentManager mgmr) {
        show(mgmr, tag);
    }

    public static <T extends BaseDialogFragment> T getInstanceOfType(FragmentManager manager, String tag) {
        Fragment fragment = manager.findFragmentByTag(tag);
        if (fragment != null && fragment instanceof DialogFragment) {
            return (T) fragment;
        }
        return null;
    }

    public static DialogFragment getInstance(FragmentManager manager, String tag) {
        Fragment fragment = manager.findFragmentByTag(tag);
        if (fragment != null && fragment instanceof DialogFragment) {
            return (DialogFragment) fragment;
        }
        return null;
    }

    public static void dismiss(FragmentManager manager,
                               String tag) {
        DialogFragment dialogFragment = getInstance(manager, tag);
        if (dialogFragment != null) {
            dialogFragment.dismissAllowingStateLoss();
        }
    }

    public AlertDialog.Builder getDefaultBuilder() {
        Bundle args = getArguments();
        String message = args.getString(EXTRA_MESSAGE);
        String title = args.getString(EXTRA_TITLE);
        boolean cancelable = args.getBoolean(EXTRA_CANCELABLE);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (title != null) {
            builder.setTitle(title);
        }
        builder.setMessage(message);
        setCancelable(cancelable);

        return builder;
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setDismissMessage(null);
        super.onDestroyView();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        postEvent(new DismissDialogEvent(getFragmentTag(), false));
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        postEvent(new DismissDialogEvent(getFragmentTag(), true));
    }
}
