package se.davison.autoflasher.ui;

import java.io.File;
import java.io.Serializable;

import se.davison.autoflasher.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;


public class FileExplorerActivity extends BaseActivity {
    public static final int REQUEST_CODE_FILE_RESULT = 99;
    private static final String TAG = FileExplorerActivity.class.getSimpleName();
    private static final String EXTRA_FILE = "extra_file";

    private Toolbar toolbar;
    private CoordinatorLayout coordinatorLayout;

    public CoordinatorLayout getCoordinatorLayout() {
        return coordinatorLayout;
    }



    //private static final String EXTRA_STORAGE_PATHS =  "extra_storage_paths";;

    @SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);



        setContentView(R.layout.activity_fragment_wrapper);

        toolbar = getActionBarToolbar();
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.cnt_fragment);

        Log.d(TAG,"==== ACTIVITY ON CREATE==");
        toolbar.setNavigationIcon(R.drawable.ic_toolbar_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        getSupportActionBar().setTitle(getString(R.string.select_zip));


        if (savedInstanceState == null) {
            File extraFile = null;
            if(getIntent().hasExtra(EXTRA_FILE)){
                extraFile = (File) getIntent().getExtras().getSerializable(EXTRA_FILE);
            }
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.cnt_fragment, FileExplorerFragment.newInstance(extraFile))
                    .commit();
        }

	}


    @Override
	public void onBackPressed() {
        ((FileExplorerFragment)getSupportFragmentManager().findFragmentById(R.id.cnt_fragment)).onBackPressed();
	}



    public static void startActivity(Activity activity, File file) {
        Intent intent = new Intent(activity, FileExplorerActivity.class);
        intent.putExtra(EXTRA_FILE, file);
        activity.startActivityForResult(intent, REQUEST_CODE_FILE_RESULT);
    }



	/*public static void startActivity(Fragment fragment, File file) {
		Intent intent = new Intent(fragment.getActivity(), FileExplorerActivity.class);
		intent.putExtra(EXTRA_FILE, file);
        fragment.startActivityForResult(intent, REQUEST_CODE_FILE_RESULT);
	}*/

    public void onFileClicked(File file) {
        Intent data = new Intent();
        data.putExtra(MainFragment.INTENT_KEY_FILE, file);
        if (getParent() == null) {
            setResult(Activity.RESULT_OK, data);
        } else {
            getParent().setResult(Activity.RESULT_OK, data);
        }
        finish();
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		getMenuInflater().inflate(R.menu.menu_file_browser, menu);
//		return super.onCreateOptionsMenu(menu);
//	}



}
