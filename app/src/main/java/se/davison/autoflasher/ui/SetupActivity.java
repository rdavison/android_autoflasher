package se.davison.autoflasher.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.prefs.Preferences;

import se.davison.autoflasher.R;
import se.davison.autoflasher.utils.Recovery;

/**
 * Created by richard on 24/01/15.
 */
public class SetupActivity extends AppCompatActivity {


    int recoveryIndex = Recovery.TWRP;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);


        final Button btnContinue = (Button) findViewById(R.id.btn_ok);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefs.edit().putInt(MainFragment.PREF_KEY_RECOVERY, recoveryIndex).commit();
                finish();
                MainActivity.startActivity(SetupActivity.this);

            }
        });

        String[] recoveries = new String[]{"TWRP", "PhilZ Touch", "ClockworkMod Recovery"};

        //TextView txtRecoveryInfo = (TextView) findViewById(R.id.txt_recovery);
        //txtRecoveryInfo.setText(Html.fromHtml(getString(R.string.chose_recovery_description)));

            NumberPicker picker = (NumberPicker) findViewById(R.id.pkr_recovery);
            setNumberPickerTextColor(picker, Color.WHITE);
            picker.setValue(0);
            picker.setMaxValue(0);
            picker.setDisplayedValues(recoveries);
            picker.setMaxValue(-1 + recoveries.length);
            picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker numberPicker, int oldVal, int newVal) {
                    if(newVal == 0 || newVal == 1){
                        recoveryIndex = Recovery.TWRP;
                    }
                    if(newVal == 2){
                        recoveryIndex = Recovery.CWM;
                        //imageSwitcher.setImageResource(R.drawable.background_setup_sl);
                    }
                }
            });


        CheckBox chkAccept = (CheckBox)findViewById(R.id.chk_accept);
        chkAccept.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                btnContinue.setEnabled(isChecked);
            }
        });





    }

    public static boolean setNumberPickerTextColor(NumberPicker numberPicker, int color)
    {
        final int count = numberPicker.getChildCount();
        for(int i = 0; i < count; i++){
            View child = numberPicker.getChildAt(i);
            if(child instanceof EditText){
                try{
                    Field selectorWheelPaintField = numberPicker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
//                    Field selectionDeivderField = numberPicker.getClass()
//                            .getDeclaredField("mSelectionDivider");
//
//                    selectionDeivderField.set(numberPicker, divider);


                    ((Paint)selectorWheelPaintField.get(numberPicker)).setColor(color);
                    ((EditText)child).setTextColor(color);
                    child.setEnabled(false);
                    numberPicker.invalidate();
                    return true;
                }
                catch(NoSuchFieldException e){
                    Log.w("setNum", e);
                }
                catch(IllegalAccessException e){
                    Log.w("setNum", e);
                }
                catch(IllegalArgumentException e){
                    Log.w("setNum", e);
                }
            }
        }
        return false;
    }


    public static void startActivity(Context context) {
        Intent intent = new Intent(context, SetupActivity.class);
        context.startActivity(intent);
    }
}
