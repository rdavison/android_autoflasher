package se.davison.autoflasher.ui;

import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.stericson.RootTools.RootTools;

import se.davison.autoflasher.R;
import se.davison.autoflasher.ui.dialogs.BaseDialogFragment;
import se.davison.autoflasher.ui.dialogs.ConfirmDialog;

/**
 * Created by richard on 2017-02-17.
 */
public class RootFragment extends Fragment implements BaseDialogFragment.DialogCallback {


    public static final int DENIED = -1;
    public static final int NOT_AVAILABLE = 0;
    public static final int GRANTED = 1;
    private static final String DIALOG_TAG_ROOT_ACCESS = "dialog_tag_root_access";


    protected boolean showRequireRootDialog() {
        if(!RootTools.isAccessGiven()) {
            if (ConfirmDialog.getInstance(getFragmentManager(), DIALOG_TAG_ROOT_ACCESS) == null) {
                new ConfirmDialog.Builder(this, getString(R.string.ok), getString(R.string.cancel)).tag(DIALOG_TAG_ROOT_ACCESS).cancelable(false).message(getString(R.string.allow_root_access)).show();
            }
            return false;
        }
        return true;
    }



    @Override
    public void onDialogCallback(BaseDialogFragment.DialogEvent event) {

        if(event instanceof ConfirmDialog.DialogEvent && event.tag.equals(DIALOG_TAG_ROOT_ACCESS)) {
            if (event.result == ConfirmDialog.ConfirmDialogEvent.YES) {
                if (RootTools.isAccessGiven()) {
                    onRootAccess(GRANTED);
                } else {
                    Toast.makeText(getActivity(), R.string.no_root, Toast.LENGTH_LONG).show();
                    onRootAccess(NOT_AVAILABLE);
                }
            } else {
                onRootAccess(DENIED);
            }
        }
    }

    protected void onRootAccess(int status) {

    }
}
