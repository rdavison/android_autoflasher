package se.davison.autoflasher.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.File;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subjects.PublishSubject;
import se.davison.autoflasher.R;
import se.davison.autoflasher.adapters.FileBrowserAdapter;
import se.davison.autoflasher.ui.dialogs.BaseDialogFragment;
import se.davison.autoflasher.utils.ExternalStorage;
import se.davison.autoflasher.utils.FileUtils;
import se.davison.autoflasher.utils.ProcessExecutor;
import se.davison.autoflasher.utils.RxPermissions;
import se.davison.autoflasher.utils.SeparatorItemDecoration;
import se.davison.autoflasher.widget.RecyclerViewFastScroller;

/**
 * Created by richard on 17/01/15.
 */
public class FileExplorerFragment extends RootFragment implements BaseDialogFragment.DialogCallback {

    private static final int REQ_PERMISSIONS = 101;
    private static final String TAG = FileExplorerFragment.class.getSimpleName();

    private FileBrowserAdapter adapter;
    private SharedPreferences prefs;
    private List<String> storages;
    private MenuItem menuItemStorage;


    public static final String EXTRA_FILE = "extra_file";


    private PublishSubject<Void> onCreateSubject = PublishSubject.create();
    private static final String PREF_KEY_LAST_DIRECTORY = "pref_key_last_directory";
    private RecyclerView recyclerView;
    private boolean activityCreated = false;
    private Snackbar snackbar;
    private File currentDirectory;
    private File originFile;
    private boolean permissionsGranted = false;

    public static FileExplorerFragment newInstance(File file){
        FileExplorerFragment fragment = new FileExplorerFragment();
        Bundle args = new Bundle(1);
        if(file!=null){
            args.putSerializable(EXTRA_FILE,file);
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        storages = ExternalStorage.getStorageDirectories(getContext(), true);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        if(!getArguments().isEmpty()){
            originFile = (File) getArguments().getSerializable(EXTRA_FILE);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }




    public void onBackPressed(){
        getActivity().finish();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        Log.d(TAG,"==== VIEW CREATED  === ");

        View root = inflater.inflate(R.layout.fragment_file_explorer, container, false);

        recyclerView = (RecyclerView)root.findViewById(android.R.id.list);
        recyclerView.addItemDecoration(new SeparatorItemDecoration(72, 0, 0x1f000000));



        adapter = new FileBrowserAdapter(getActivity(), getString(R.string.up_to),storages);

        recyclerView.setAdapter(adapter);

        final RecyclerViewFastScroller fastScroller = (RecyclerViewFastScroller) root.findViewById(R.id.fastscroller);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false) {
            @Override
            public void onLayoutChildren(final RecyclerView.Recycler recycler, final RecyclerView.State state) {
                super.onLayoutChildren(recycler, state);
                //TODO if the items are filtered, considered hiding the fast scroller here
                final int firstVisibleItemPosition = findFirstVisibleItemPosition();
                if (firstVisibleItemPosition != 0) {
                    // this avoids trying to handle un-needed calls
                    if (firstVisibleItemPosition == -1)
                        //not initialized, or no items shown, so hide fast-scroller
                        fastScroller.setVisibility(View.GONE);
                    return;
                }
                final int lastVisibleItemPosition = findLastVisibleItemPosition();
                int itemsShown = lastVisibleItemPosition - firstVisibleItemPosition + 1;
                //if all items are shown, hide the fast-scroller
                fastScroller.setVisibility(adapter.getItemCount() > itemsShown ? View.VISIBLE : View.GONE);
            }
        });
        fastScroller.setRecyclerView(recyclerView);
        fastScroller.setViewsToUse(R.layout.recycler_view_fast_scroller__fast_scroller, R.id.fastscroller_bubble, R.id.fastscroller_handle);


        adapter.setOnItemClickListener(new FileBrowserAdapter.FileBrowserAdapterClickListener() {
            @Override
            public void onItemClick(View o, int position, File file, boolean isFolder) {
                if (file.isDirectory() || isFolder) {
                    setDirectory(file);

                } else {

                    prefs.edit().putString(PREF_KEY_LAST_DIRECTORY, adapter.getCurrentDirectory().getAbsolutePath()).apply();
                    Activity activity = getActivity();
                    Intent data = new Intent();
                    data.putExtra(MainFragment.INTENT_KEY_FILE, file);
                    data.putExtra(MainFragment.INTENT_KEY_ORIGIN_FILE, originFile);
                    if (activity.getParent() == null) {
                        activity.setResult(Activity.RESULT_OK, data);
                    } else {
                        activity.getParent().setResult(Activity.RESULT_OK, data);
                    }
                    activity.finish();
                }
            }
        });

        return root;

    }

    private void setDirectory(File file) {
        currentDirectory = file;
        if(ExternalStorage.isRoot(storages, file)){
            if(showRequireRootDialog()){
                adapter.setDirectory(file, true);
                updateActionBarTitle(file);
            }
        }else{
            adapter.setDirectory(file);
            updateActionBarTitle(file);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        activityCreated = false;
        Log.d("TAG", "ON PAUSE");
    }

    @Override
    public void onResume() {
        activityCreated = true;
        if(!permissionsGranted) {
            onCreateSubject.onNext(null);
        }
        super.onResume();
//        if(permissionsGranted) {
//            setDirectory(currentDirectory);
//        }
        Log.d("TAG", "ON RESUME");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onCreateSubject.onNext(null);
        activityCreated = true;

        Log.d("TAG", "ON ATTACH");
        checkPermissions();

    }

    private void checkPermissions() {
       RxPermissions.get(getActivity())
                .request(mPermissionsRequester,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE).subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Boolean>() {

                    private void handle(boolean granted) {
                        if (granted) {
                            allPermissionsGranted();
                        } else {
                            showPermissionSnackbar();
                        }
                    }

                    @Override
                    public void call(final Boolean granted) {
                        if (!activityCreated) {
                            onCreateSubject.subscribe(new Action1<Void>() {
                                @Override
                                public void call(Void aVoid) {
                                    handle(granted);
                                }
                            });
                        } else {
                            handle(granted);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
    }

    private void showPermissionSnackbar() {

        CoordinatorLayout parent = ((FileExplorerActivity)getActivity()).getCoordinatorLayout();

        if(parent!=null) {
            snackbar = Snackbar
                    .make(((FileExplorerActivity) getActivity()).getCoordinatorLayout(), getString(R.string.change_permissions), Snackbar.LENGTH_INDEFINITE)
                    .setAction(getString(R.string.settings).toUpperCase(), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            RxPermissions.showInstalledAppDetails(FileExplorerFragment.this);
                        }
                    });

            snackbar.show();
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==RxPermissions.REQ_CODE_SETTINGS){
            checkPermissions();
        }

    }

    private void allPermissionsGranted() {

        if(snackbar!=null){
            snackbar.dismiss();
        }

        permissionsGranted = true;


        String lastPath = prefs.getString(PREF_KEY_LAST_DIRECTORY, storages.get(ExternalStorage.SD_CARD));
        if(lastPath==null){
            lastPath = ExternalStorage.getSdCardPath();
        }
        
        
        File startFile = new File(lastPath);
        if(originFile !=null){
            startFile = originFile.getParentFile();
        }

        String sdCardPath = ExternalStorage.getSdCardPath();
        boolean exists;
        if(ExternalStorage.isRoot(storages,startFile)){
            exists = FileUtils.exists(startFile, true);
        }else{
            exists = startFile.exists();
        }
        if (!exists) {
            startFile = new File(sdCardPath);
            exists = startFile.exists();
        }

        if (!exists || !ExternalStorage.isAvailable()) {
            Toast.makeText(getActivity(), R.string.sd_card_fail, Toast.LENGTH_LONG).show();
            getActivity().finish();
        }

        setDirectory(startFile);
    }

    private void updateActionBarTitle(File file) {
        ((AppCompatActivity)getActivity()).getSupportActionBar().setSubtitle(file.getAbsolutePath());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_file_browser,menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        if (storages.size() > 1) {
			menuItemStorage = menu.findItem(R.id.menu_switch_card);
			menuItemStorage.setVisible(true);
		}
    }


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_switch_card:
			File fileToSwitch = null;
			Drawable icon = null;
			String title = "";
			if (adapter.getCurrentDirectory().getAbsolutePath().startsWith(storages.get(ExternalStorage.SD_CARD))) {
				fileToSwitch = new File(storages.get(ExternalStorage.EXTERNAL_SD_CARD));
				icon = ContextCompat.getDrawable(getContext(),R.drawable.ic_toolbar_sdcard);
				title = getString(R.string.view_internal_sd_card);
			} else {
				fileToSwitch = new File(storages.get(ExternalStorage.SD_CARD));
				icon = ContextCompat.getDrawable(getContext(), R.drawable.ic_toolbar_external_sdcard);
				title = getString(R.string.view_external_sd_card);
			}
			if (fileToSwitch != null && fileToSwitch.exists()) {
				menuItemStorage.setIcon(icon);
				menuItemStorage.setTitle(title);
                updateActionBarTitle(fileToSwitch);
                adapter.setDirectory(fileToSwitch);
			}
		}
		return super.onOptionsItemSelected(item);
	}

    private final RxPermissions.PermissionsRequester mPermissionsRequester = new RxPermissions.PermissionsRequester() {
        @Override public void performRequestPermissions(String[] permissions) {
            // forward request to the system by calling fragment's method
            requestPermissions(permissions, REQ_PERMISSIONS);
        }
    };


    @Override
    public void onRequestPermissionsResult(int requestCode,final String[] permissions, final int[] grantResults) {

        if (requestCode == REQ_PERMISSIONS) {
            mPermissionsRequester.onRequestPermissionsResult(permissions, grantResults);
        }
    }


    @Override
    protected void onRootAccess(int status) {
        if(status == RootFragment.GRANTED){
            adapter.setDirectory(currentDirectory);
            updateActionBarTitle(currentDirectory);
        }
    }
}
