package se.davison.autoflasher.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.github.lukaspili.reactivebilling.ReactiveBilling;
import com.github.lukaspili.reactivebilling.model.PurchaseType;
import com.github.lukaspili.reactivebilling.response.GetPurchasesResponse;
import com.github.lukaspili.reactivebilling.response.PurchaseResponse;
import com.github.lukaspili.reactivebilling.response.Response;

import java.util.Random;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;
import rx.subscriptions.CompositeSubscription;
import se.davison.autoflasher.R;
import se.davison.autoflasher.utils.PurchaseException;
import se.davison.autoflasher.utils.ReminderDialogFragment;
import se.davison.autoflasher.utils.Utils;


public class MainActivity extends BaseActivity implements ReminderDialogFragment.Callback {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String SKU_PREMIUM = "sku_premium";
    private static final String BILL_PAYLOAD = "bill_payload";
    private static final int RC_REQUEST = 1;
    private static final String DIALOG_TAG_RATE = "dialog_tag_rate";
    private static final String DIALOG_TAG_PREMIUM = "dialog_tag_premium";
    private static final String APP_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAls4osFjoQBI200UJ1hLA2v7bVYG2oWf3brN9DYiXRDZFJBTMxZwnvVgzgixoJu/u21lGWK53CyEJRPT6t2ziZSlWUJKRqz311/+SidvY3eMIS6EE0u5ILRgmduGSYLzc+HpPqqsxPHL8ZcN3fTVywxwLNcEixRBdVBmVlgzjy5mf2fXElkmp7B4ME74oUuZcUKCYqaYzkP+0CpfhGZFxkE8rthpdfzHO0kuswDOliHvRtvIjzfPuVIQral24LZdtm1dsgoPKIDxoWjmyAE+UbSzs71g63Pmyvq4tWfUCSG06XfS/8RzDjdue2jQqWu0xJ43aOLPR/gZcjccrsllQ1QIDAQAB";


    private SharedPreferences prefs;
    private CompositeSubscription subscriptions;
    private MenuItem menuItemPremium;

    private boolean billingSupported = false;
    private boolean isPremium = false;

    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "ON_START");
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e(TAG, "ON_CREATE");

        subscriptions = new CompositeSubscription();


        ReactiveBilling.getInstance(this).isBillingSupported(PurchaseType.PRODUCT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Response>() {
                    @Override
                    public void call(final Response response) {
                        handleBillingSupported(response.isSuccess());
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        handleBillingSupported(false);
                    }
                });


        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        if (!isTaskRoot()) {
            final Intent intent = getIntent();
            final String intentAction = intent.getAction();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && intentAction != null && intentAction.equals(Intent.ACTION_MAIN)) {
                finish();
                return;
            }
        }

        //TODO remove
        prefs.edit().putInt(MainFragment.PREF_KEY_RECOVERY, 0).commit();

        if (savedInstanceState == null) {
            new ReminderDialogFragment.Builder(this, getSupportFragmentManager(), DIALOG_TAG_RATE, 3).message(getString(R.string.please_rate_message)).okTitle(getString(R.string.sure)).dismissTitle(getString(R.string.maybe_later)).repeat(5, getString(R.string.dont_show_again)).show();

        }


        setContentView(R.layout.activity_fragment_wrapper);

        Toolbar toolbar = getActionBarToolbar();
        toolbar.setTitle(getString(R.string.app_name));
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                switch (menuItem.getItemId()) {
                    case R.id.menu_add:
                        FileExplorerActivity.startActivity(MainActivity.this, null);

                        return true;
                        /*case R.id.menu_setup:
                            SetupActivity.startActivity(MainActivity.this);
                            return true;*/
                    case R.id.menu_premium:

                        purchasePremium();
                        return true;
                }

                return false;
            }
        });


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.cnt_fragment, MainFragment.newInstance())
                    .commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "ON_RESUME");
        isPremium = Utils.isPremium(prefs, MainActivity.this);
    }

    private void handleBillingSupported(boolean success) {
        billingSupported = success;
        if (billingSupported) {
            if (!isPremium) {
                showPurchaseReminderDialog();
                toggelPurchaseMenu(true);
            }
            loadPurchases();
            loadPurchaseFlow();
        } else {
            toggelPurchaseMenu(false);
        }
    }

    private void loadPurchaseFlow() {
        subscriptions.add(ReactiveBilling.getInstance(this).purchaseFlow().flatMap(new Func1<PurchaseResponse, Observable<Boolean>>() {
            @Override
            public Observable<Boolean> call(PurchaseResponse purchaseResponse) {
                Log.i(TAG, "purchaseFlow()");
                return observableFromIAPResponse(purchaseResponse);
            }
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean hasProduct) {
                        handleProductPurchase(hasProduct, true);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        //reload purchase flow and add subscription
                        loadPurchaseFlow();
                        Log.d(TAG, "ERROR: purchaseFlow()");
                        handlePurchaseError(throwable);
                    }
                }));
    }

    private void loadPurchases() {
        subscriptions.add(ReactiveBilling.getInstance(this)
                .getPurchases(PurchaseType.PRODUCT, null)
                .flatMap(new Func1<GetPurchasesResponse, Observable<Boolean>>() {
                    @Override
                    public Observable<Boolean> call(GetPurchasesResponse getPurchasesResponse) {
                        return observableFromIAPResponse(getPurchasesResponse);
                    }
                }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean hasProduct) {
                        handleProductPurchase(hasProduct, false);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        handlePurchaseError(null);
                    }
                }));
    }

    private void toggelPurchaseMenu(boolean visible) {
        if (menuItemPremium != null) {
            menuItemPremium.setVisible(visible);
        }
    }

    private void showPurchaseReminderDialog() {
        new ReminderDialogFragment.Builder(MainActivity.this, getSupportFragmentManager(), DIALOG_TAG_PREMIUM, 5).message(getString(R.string.please_buy_message)).okTitle(getString(R.string.ok)).dismissTitle(getString(R.string.maybe_later)).repeat(10, getString(R.string.dont_show_again)).show();
    }

    private Observable<Boolean> observableFromIAPResponse(final Response response) {

        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {

                if (response.isSuccess()) {
                    if (response instanceof GetPurchasesResponse) {
                        for (GetPurchasesResponse.PurchaseResponse purchase : ((GetPurchasesResponse) response).getList()) {
                            if (purchase.getProductId().equals(SKU_PREMIUM)) {
                                subscriber.onNext(true);
                                return;
                            }
                        }
                        subscriber.onNext(false);
                        return;
                    } else {
                        subscriber.onNext(true);
                        return;
                    }

                }
                subscriber.onError(new PurchaseException(response.getResponseCode()));
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (subscriptions != null) {
            subscriptions.unsubscribe();
            subscriptions = null;
        }

        super.onDestroy();
    }

    private Boolean purchaseInProgress = false;

    private void purchasePremium() {
        if (billingSupported && !purchaseInProgress) {
            purchaseInProgress = true;
            subscriptions.add(ReactiveBilling.getInstance(this).startPurchase(SKU_PREMIUM, PurchaseType.PRODUCT, null, null)
                    .flatMap(new Func1<Response, Observable<Boolean>>() {
                        @Override
                        public Observable<Boolean> call(Response response) {
                            return observableFromIAPResponse(response);
                        }
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<Boolean>() {
                        @Override
                        public void call(Boolean response) {
                            purchaseInProgress = false;
                            Log.i(TAG, "startPurchase()");
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            purchaseInProgress = false;
                            Log.d(TAG, "ERROR: startPurchase()");
                            handlePurchaseError(throwable);
                        }
                    }));

        }
    }

    private void handleProductPurchase(boolean owned, boolean purchased) {
        Log.i(TAG, "Is premium owned?:" + owned);
        Utils.setPremium(prefs, MainActivity.this, owned);
        if (menuItemPremium != null) {
            toggelPurchaseMenu(!owned);
        }
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.cnt_fragment);
        if (fragment != null) {
            ((MainFragment) fragment).handlePremium(owned);
        }
        if (owned && purchased) {
            Toast.makeText(this, R.string.premium_purchased, Toast.LENGTH_LONG).show();
        }
    }

    private void handlePurchaseError(Throwable throwable) {
        String errorMessage = getString(R.string.err_purchase);
        if (throwable instanceof PurchaseException) {
            int responseCode = ((PurchaseException) throwable).getResponseCode();
            switch (responseCode) {
                case -1:
                    errorMessage = null;
                    break;
                case 7:
                    handleProductPurchase(true, false);
                    errorMessage = getString(R.string.item_owned);
                    break;
            }
        }
        throwable.printStackTrace();
        if(errorMessage!=null){
            Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menuItemPremium = menu.findItem(R.id.menu_premium);
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.cnt_fragment);
        fragment.onActivityResult(requestCode, resultCode, data);
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void onReminderDialogAccept(String tag) {
        if (tag.equals(DIALOG_TAG_RATE)) {
            final String appPackageName = getPackageName();
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        }
        if (tag.equals(DIALOG_TAG_PREMIUM)) {
            purchasePremium();
        }

    }


    @Override
    public void onReminderDialogDismiss(String tag) {

    }
}
