package se.davison.autoflasher.ui;


import android.app.Dialog;
import android.app.backup.BackupManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.emtronics.dragsortrecycler.DragSortRecycler;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedHashMap;

import se.davison.autoflasher.R;
import se.davison.autoflasher.adapters.RulesAdapter;
import se.davison.autoflasher.model.FlashRule;
import se.davison.autoflasher.utils.JsonUtils;
import se.davison.autoflasher.utils.Utils;


/**
 * Created by richard on 17/01/15.
 */
public class MainFragment extends Fragment {

    public static final String INTENT_KEY_FILE = "intent_key_file";
    public static final String INTENT_KEY_ORIGIN_FILE = "intent_key_origin_file";
    private static final String TAG = MainFragment.class.getSimpleName();
    private static final String FRAGMENT_TAG_EDIT_DIALOG = "fragment_tag_edit_dialog";
    private static final String PREF_KEY_FLASH_RULES = "pref_key_auto_flasher_rules";
    public static final String PREF_KEY_RECOVERY = "pref_key_recovery";

    private RulesAdapter adapter;
    private SharedPreferences prefs;
    private View emptyView;
    private RecyclerView recyclerView;
    private ActionMode actionMode;
    private Button btnFlash;
    private DragSortRecycler dragSortRecycler;
    private boolean isPremium = false;
    private AdView adView;
    private int defaultStatusbarColor;
    private ActionMode.Callback actionModeCallback;
    private MenuItem menuItemAdd;
    private BackupManager backupManager;


    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);

        Context context = getActivity();

        backupManager = new BackupManager(context);

        prefs = PreferenceManager.getDefaultSharedPreferences(context);

        if (Utils.isApi(Build.VERSION_CODES.LOLLIPOP)) {
            TypedValue colorPrimaryDark = new TypedValue();
            context.getTheme().resolveAttribute(android.R.attr.colorPrimaryDark, colorPrimaryDark, true);
            defaultStatusbarColor = ContextCompat.getColor(getContext(),colorPrimaryDark.resourceId);
        }

        adapter = new RulesAdapter(getActivity());
        adapter.setHasStableIds(true);
        adapter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Log.d(TAG, "----click no "+position);
                if (!adapter.inSelectionMode()) {
                    editRule(adapter.getItems().get(position), false, position);
                } else {
                    if(adapter.getSelectionCount()==0){
                        actionMode.finish();
                        doneClicked();
                    }else{
                        if (actionMode != null) {
                            actionMode.setTitle("" + adapter.getSelectionCount());
                        }
                    }
                }
            }
        });
        adapter.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {



                adapter.setSelectionMode(true);
                adapter.setItemSelected(position, true);

                actionModeCallback = new ActionMode.Callback() {

                    String oldtitle;

                    @Override
                    public boolean onCreateActionMode(ActionMode mode, Menu menu) {

                        Toolbar toolbar = ((BaseActivity) getActivity()).getActionBarToolbar();
                        oldtitle = toolbar.getTitle().toString();
                        toolbar.setTitle("");


                        dragSortRecycler.setEnabled(false);
                        menuItemAdd.setEnabled(false);
                        menuItemAdd.setVisible(false);
                        actionMode = mode;
                        mode.setTitle("1");
                        MenuInflater inflater = mode.getMenuInflater();
                        inflater.inflate(R.menu.action_mode, menu);


                        setStatusbarColor(0xFFB3B3B3);

                        return true;
                    }

                    @Override
                    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                        return false;
                    }

                    @Override
                    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_delete:
                                deleteClicked();
                                return true;
                            default:
                                doneClicked();
                                return true;
                        }
                    }


                    @Override
                    public void onDestroyActionMode(ActionMode mode) {

                        doneClicked();
                        actionMode = null;
                        menuItemAdd.setEnabled(true);
                        menuItemAdd.setVisible(true);
                        Toolbar toolbar = ((BaseActivity) getActivity()).getActionBarToolbar();
                        toolbar.setTitle(oldtitle);
                    }
                };

                AppCompatActivity actionBarActivity = (AppCompatActivity)getActivity();

                actionBarActivity.startSupportActionMode(actionModeCallback);

                return true;
            }
        });
        
        try{
            ArrayList items = JsonUtils.loadJson(prefs, PREF_KEY_FLASH_RULES,new TypeToken<ArrayList<FlashRule>>() {});
            adapter.setItems(items);
        }catch (Exception e){
            prefs.edit().remove(PREF_KEY_FLASH_RULES).commit();
            backupManager.dataChanged();
            Toast.makeText(getActivity(), getString(R.string.err_read_rules), Toast.LENGTH_SHORT).show();
        }
    
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menuItemAdd = menu.findItem(R.id.menu_add);
        super.onPrepareOptionsMenu(menu);
    }

    public void handlePremium(boolean isPremium){
        if(adView!=null) {
            Utils.setupAds(adView, isPremium);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_main, container, false);

        isPremium = Utils.isPremium(prefs,getActivity());

        adView = (AdView) view.findViewById(R.id.ad_view);
        Utils.setupAds(adView, isPremium);

        TextView emptyText = (TextView) view.findViewById(R.id.txt_list_empty);
        Spanned html;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            html = Html.fromHtml(getString(R.string.rules_empty),Html.FROM_HTML_MODE_LEGACY);
        } else {
            html = Html.fromHtml(getString(R.string.rules_empty));
        }
        emptyText.setText(html);


        btnFlash = (Button) view.findViewById(R.id.btn_flash);
        btnFlash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FlashActivity.startActivity(getActivity(), adapter.getItems());
            }
        });

        emptyView = view.findViewById(R.id.lay_empty);


        recyclerView = (RecyclerView) view.findViewById(R.id.lst_rules);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));






        recyclerView.setItemAnimator(null);

        checkEmpty();

        dragSortRecycler = new DragSortRecycler();
        dragSortRecycler.setViewHandleId(R.id.img_drag);
        dragSortRecycler.setFloatingAlpha(1f);
        //dragSortRecycler.setFloatingBgColor(0x1F000000);
        dragSortRecycler.setFLoatingBgDrawable(R.drawable.sort_background);
        dragSortRecycler.setAutoScrollSpeed(0.3f);
        dragSortRecycler.setAutoScrollWindow(0.1f);

        dragSortRecycler.setOnItemMovedListener(new DragSortRecycler.OnItemMovedListener() {
            @Override
            public void onItemMoved(int from, int to) {
                FlashRule rule = adapter.getItems().remove(from);
                adapter.getItems().add(to, rule);
                adapter.notifyDataSetChanged();
                saveItems();
                //notifyItemMoved does work, but it makes the list scroll pos jump a little when dragging near the top or bottom
                //adapter.notifyItemMoved(from,to);
            }
        });

        dragSortRecycler.setOnDragStateChangedListener(new DragSortRecycler.OnDragStateChangedListener() {
            @Override
            public void onDragStart() {
                Log.d(TAG, "Drag Start");
            }

            @Override
            public void onDragStop() {
                Log.d(TAG, "Drag Stop");
            }
        });

        recyclerView.addItemDecoration(dragSortRecycler);
        recyclerView.addOnItemTouchListener(dragSortRecycler);
        recyclerView.addOnScrollListener(dragSortRecycler.getScrollListener());
        recyclerView.setAdapter(adapter);


        return view;
    }



    private void setStatusbarColor(int color) {
        if (Utils.isApi(Build.VERSION_CODES.LOLLIPOP)) {
            Window window = getActivity().getWindow();
            window.setStatusBarColor(color);
        }
    }

    private void deleteClicked() {
        adapter.getItems().removeAll(adapter.getSelectedItems());
        actionMode.finish();
        doneClicked();
        saveItems();
        checkEmpty();
    }

    private void doneClicked() {
        setStatusbarColor(defaultStatusbarColor);
        adapter.setSelectionMode(false);
        dragSortRecycler.setEnabled(true);
    }

    private void checkEmpty() {
        if (adapter.getItemCount() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            btnFlash.setEnabled(false);
        } else {
            emptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            btnFlash.setEnabled(true);
        }
    }

    private void saveItems() {
        JsonUtils.saveJson(prefs, PREF_KEY_FLASH_RULES, adapter.getItems(), false);
        backupManager.dataChanged();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        if(actionMode!=null){
            ((AppCompatActivity)getActivity()).startSupportActionMode(actionModeCallback);
        }
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==FileExplorerActivity.REQUEST_CODE_FILE_RESULT && data != null) {
            File file = (File) data.getSerializableExtra(INTENT_KEY_FILE);
            File originFile = (File)data.getSerializableExtra(INTENT_KEY_ORIGIN_FILE);
            FlashRule.NameRule nameRule = FlashRule.NameRule.BEGINS_WITH;
            int position = -1;
            if(originFile!=null){
                int index = 0;
                for(FlashRule item : adapter.getItems()){
                    if(item.getFile().equals(originFile)){
                        position = index;
                        nameRule = item.getNameRule();
                        break;
                    }
                    index++;
                }
            }

            editRule(new FlashRule(file, file.getName(), nameRule), originFile==null, position);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.d(TAG,"=====CONFIG CHANGE===");
        super.onConfigurationChanged(newConfig);
    }

    private void editRule(FlashRule flashRule, boolean newFile, int position) {
        EditRuleDialogFragment fragment = EditRuleDialogFragment.newInstance(flashRule, newFile, position);
        fragment.setTargetFragment(this,0);
        fragment.show(getFragmentManager(), FRAGMENT_TAG_EDIT_DIALOG);
    }

    public static class EditRuleDialogFragment extends DialogFragment {

        public FlashRule.NameRule currentRule;

        private static String EXTRA_FLASH_RULE = "extra_flash_rule";
        private static String EXTRA_NEW_FILE = "extra_new_file";
        private static String EXTRA_POSITION = "extra_position";

        public static EditRuleDialogFragment newInstance(FlashRule flashRule, boolean newFile, int position) {
            EditRuleDialogFragment fragment = new EditRuleDialogFragment();
            Bundle args = new Bundle();
            args.putSerializable(EXTRA_FLASH_RULE, flashRule);
            args.putBoolean(EXTRA_NEW_FILE, newFile);
            args.putInt(EXTRA_POSITION, position);
            fragment.setArguments(args);
            return fragment;
        }




        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            Context context = getActivity();

            final FlashRule rule = (FlashRule) getArguments().getSerializable(EXTRA_FLASH_RULE);
            final boolean newFile = getArguments().getBoolean(EXTRA_NEW_FILE);
            final int position = getArguments().getInt(EXTRA_POSITION);

            int dp16 = Utils.dpToPx(16, context);
            int dp24 = Utils.dpToPx(24, context);

            final LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setPadding(dp24, 0, dp24, 0);

            final TextView txtMessage = new TextView(context);
            txtMessage.setText(getString(R.string.name_prefix_description));
            txtMessage.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            txtMessage.setPadding(0, dp16, 0, dp16);

            final EditText input = new EditText(context);
            input.append(rule.getMatchPhrase());

            final LinkedHashMap<FlashRule.NameRule, String> ruleMap = new LinkedHashMap<>();
            ruleMap.put(FlashRule.NameRule.BEGINS_WITH, getString(R.string.begins_with));
            ruleMap.put(FlashRule.NameRule.ENDS_WITH, getString(R.string.ends_with));
            ruleMap.put(FlashRule.NameRule.CONTAINS, getString(R.string.contains));
            ruleMap.put(FlashRule.NameRule.IS_EXACTLY, getString(R.string.is_exactly));
            ruleMap.put(FlashRule.NameRule.REGEXP, getString(R.string.regexp));

            final Spinner spinner = new Spinner(context);
            spinner.setAdapter(new ArrayAdapter<String>(context,
                    android.R.layout.simple_list_item_1,ruleMap.values().toArray(new String[ruleMap.size()])) {
            });
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                public void onItemSelected(AdapterView<?> parent, View v, int pos,
                                           long id) {

                    int i = 0;
                    for(FlashRule.NameRule nr : ruleMap.keySet()){
                        currentRule = nr;
                        if(i == pos) {
                            break;
                        }
                        i++;
                    }
                }

                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            int selection = 0;
            for(FlashRule.NameRule nr : ruleMap.keySet()){
                if(nr == rule.getNameRule()) {
                    break;
                }
                selection++;
            }

            spinner.setSelection(selection);

            layout.addView(txtMessage);
            layout.addView(spinner);
            layout.addView(input);

            String title = newFile ? getString(R.string.name_rule)
                    : getString(R.string.name_rule_edit);

            //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    
            AlertDialog.Builder buidler = new AlertDialog.Builder(getActivity())
                    .setTitle(title)
                    .setView(layout)
                    .setPositiveButton(newFile ? getString(R.string.add) : getString(R.string.save), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            Editable text = input.getText();
                            rule.setNameRule(currentRule);
                            rule.setMatchPhrase(text.toString());
                            imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                            //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                            ((MainFragment) getTargetFragment()).onRuleAdded(new FlashRuleMessage(rule, newFile, position));
                        }
                    })
                    .setNegativeButton(getString(R.string.cancel),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                                    // Do nothing.
                                    //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                                }
                            });
            
            if(!newFile){
                buidler.setNeutralButton(getString(R.string.change_file), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                        FileExplorerActivity.startActivity(getActivity(),rule.getFile());
                    }
                });
            }
            
            return buidler.create();

        }
    }

    private static class FlashRuleMessage {

        public FlashRule rule;
        public boolean newFile;
        public int position;

        public FlashRuleMessage(FlashRule rule, boolean newFile, int position) {
            this.rule = rule;
            this.newFile = newFile;
            this.position = position;
        }
    }

    private void onRuleAdded(FlashRuleMessage ruleEvent) {
        if (ruleEvent.newFile) {
            adapter.getItems().add(ruleEvent.rule);
            adapter.notifyItemInserted(adapter.getItemCount() - 1);
        } else {
            adapter.getItems().set(ruleEvent.position, ruleEvent.rule);
            adapter.notifyItemChanged(ruleEvent.position);
        }
        checkEmpty();
        saveItems();
    }
}
