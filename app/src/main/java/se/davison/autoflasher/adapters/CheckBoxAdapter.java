package se.davison.autoflasher.adapters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.TextView;

public class CheckBoxAdapter<T> extends BaseAdapter implements OnItemClickListener {
	
	private final List<T> items;
	private final LayoutInflater inflater;
	private final int textViewResourceId;
	private final List<Boolean> checkedItems;
	private boolean isCheckedView;
	private int choiceMode;
	
	public CheckBoxAdapter(Context context, ListView listView, int textViewResourceId, List<T> items, boolean defaultValue) {
		this.items = items;
		
		int size = items.size();
		
		checkedItems = new ArrayList<Boolean>(size);
		
		fill(size, defaultValue);
		
		inflater = LayoutInflater.from(context);
		this.textViewResourceId = textViewResourceId;
		listView.setOnItemClickListener(this);
	}
	
	private void fill(int size, boolean defaultValue) {
		for (int i = 0; i < size; i++) {
			if (checkedItems.size() < size) {
				checkedItems.add(defaultValue);
			} else {
				checkedItems.set(i, defaultValue);
			}
			
		}
	}
	
	public CheckBoxAdapter(Context context, ListView listView, int textViewResourceId, List<T> items) {
		this(context, listView, textViewResourceId, items, false);
	}
	
	public void checkAll() {
		Collections.fill(checkedItems, true);
		notifyDataSetChanged();
	}
	
	public void uncheckAll() {
		Collections.fill(checkedItems, false);
		notifyDataSetChanged();
	}
	
	
	public List<T> getItemsWithValue(boolean checked) {
		List<T> newItems = new ArrayList<>(getCount());
		int i = 0;
		for (Boolean b : checkedItems) {
			if (b == checked) {
				newItems.add(items.get(i));
			}
			i++;
		}
		return newItems;
	}
	
	@Override
	public int getCount() {
		return items == null ? 0 : items.size();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = inflater.inflate(textViewResourceId, parent, false);
			if (convertView instanceof CheckedTextView) {
				isCheckedView = true;
			} else {
				holder = new ViewHolder((CheckBox) convertView.findViewById(android.R.id.checkbox), (TextView) convertView.findViewById(android.R.id.text1));
				convertView.setTag(holder);
			}
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		if (!isCheckedView) {
			holder.chkCheckbox.setChecked(checkedItems.get(position));
			holder.txtCheckbox.setText(items.get(position).toString());
		} else {
			CheckedTextView checkedTextView = ((CheckedTextView) convertView);
			checkedTextView.setText(items.get(position).toString());
			checkedTextView.setChecked(checkedItems.get(position));
		}
		
		return convertView;
	}
	
	public void setChoiceMode(int choiceMode) {
		this.choiceMode = choiceMode;
	}
	
	public void setChecked(int position, boolean checked) {
		checkedItems.set(0, checked);
	}
	
	private static class ViewHolder {
		public TextView txtCheckbox;
		public CheckBox chkCheckbox;
		
		public ViewHolder(CheckBox chkCheckbox, TextView txtCheckbox) {
			super();
			this.chkCheckbox = chkCheckbox;
			this.txtCheckbox = txtCheckbox;
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if (choiceMode == ListView.CHOICE_MODE_SINGLE) {
			fill(items.size(), false);
		}
		checkedItems.set(position, !checkedItems.get(position));
		notifyDataSetChanged();
	}
	
	@Override
	public T getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
}
