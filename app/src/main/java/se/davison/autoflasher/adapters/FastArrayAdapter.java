package se.davison.autoflasher.adapters;

/**
 * Created by richard on 19/01/15.
 */
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class FastArrayAdapter<T> extends BaseAdapter {

    private List<T> items;
    private LayoutInflater inflater;
    private int layoutId = 0;
    private boolean notifyOnChange = true;
    private Object lock = new Object();

    public FastArrayAdapter(Context context, List<T> items, int layoutId) {
        init(context, items, layoutId);
    }

    public FastArrayAdapter(Context context, List<T> items) {
        init(context, items, 0);
    }

    public void init(Context context, List<T> items, int layoutId) {
        this.items = items;
        this.inflater = LayoutInflater.from(context);
        if (layoutId == 0) {
            layoutId = android.R.layout.simple_list_item_1;
        }
        this.layoutId = layoutId;
    }

    public static <T> List<T> getAdapterItems(ArrayAdapter<T> adapter) {
        int size = adapter.getCount();
        List<T> list = new ArrayList<T>(size);
        for (int i = 0; i < size; i++) {
            list.add(adapter.getItem(i));
        }
        return list;
    }

    public static <T> void setAdapterItems(ArrayAdapter<T> adapter, List<T> items) {
        adapter.clear();
        int size = items.size();
        for (int i = 0; i < size; i++) {
            adapter.add(items.get(i));
        }
    }

    @Override
    public int getCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public T getItem(int position) {
        return items.get(position);
    }

    public void setItems(List<T> items){
        synchronized (lock) {
            this.items = items;
        }
    }

    public void remove(T object) {
        synchronized (lock) {
            if (items != null) {
                items.remove(object);
            }
        }
        if (notifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void insert(T object, int index) {
        synchronized (lock) {
            if (items != null) {
                items.add(index, object);
            }
        }
        if (notifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void add(T object) {
        synchronized (lock) {
            if (items != null) {
                items.add(object);
            }
        }
        if (notifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void clear() {
        synchronized (lock) {
            if (items != null) {
                items.clear();
            }
        }
        if (notifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public List<T> getItems() {
        return items;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        notifyOnChange = true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder((TextView) convertView.findViewById(android.R.id.text1), (TextView) convertView.findViewById(android.R.id.text2));
            if (holder.text1 == null) {
                throw new IllegalStateException("FastArrayAdapter requires the resource ID 'android.R.id.text1'");
            }
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        T item = getItem(position);

        if (holder.text1 != null && holder.text2 != null) {
            String[] textParts = item.toString().split("\n");
            if (textParts.length > 1) {
                holder.text1.setText(textParts[0]);
                holder.text2.setText(textParts[textParts.length-1]);
            } else {
                holder.text1.setText(item.toString());
            }
        } else {
            holder.text1.setText(item.toString());
        }
        return convertView;
    }

    private static class ViewHolder {
        TextView text1;
        TextView text2;

        public ViewHolder(TextView text1, TextView text2) {
            this.text1 = text1;
            this.text2 = text2;
        }
    }

}
