package se.davison.autoflasher.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import se.davison.autoflasher.R;
import se.davison.autoflasher.utils.FileUtils;
import se.davison.autoflasher.utils.ProcessExecutor;
import se.davison.autoflasher.widget.RecyclerViewFastScroller;

/**
 * Created by richard on 17/01/15.
 */
public class FileBrowserAdapter extends RecyclerView.Adapter<FileBrowserAdapter.ViewHolder> implements RecyclerViewFastScroller.BubbleTextGetter {


    @Override
    public String getTextToShowInBubble(int pos) {
        if(!isRootDirectory && pos==0){
            return "\u2B06";
        }
        String name = getItem(pos).getName();
        if (name.equals("")){
            return name;
        }
        return name.substring(0,1).toUpperCase();
    }

    public interface FileBrowserAdapterClickListener{

        void onItemClick(View o, int position, File file, boolean isFolder);
    }

    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.0");
    private static final DateFormat DATE_FORMAT = SimpleDateFormat.getDateTimeInstance();//new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private static final Comparator<File> FILE_NAME_COMPARATOR = new Comparator<File>() {
        @Override
        public int compare(File lhs, File rhs) {
            return lhs.getName().toLowerCase().compareTo(rhs.getName().toLowerCase());
        }
    };

    private File currentDirectory;
    private boolean isRootDirectory = false;
    private List<File> fileList;
    private LayoutInflater inflater;
    private SparseBooleanArray isSdCard;
    private final String strUpTo;
    private List<String> storages;
    private int folderListSize = -1;

    private Drawable icFile;
    private Drawable icFolder;
    private Drawable icZip;
    private Drawable icUp;
    private Drawable icSdCard;
    private FileBrowserAdapterClickListener listener;
    private String environmentStoragePath = null;

    public File getCurrentDirectory() {
        return currentDirectory;
    }

    public FileBrowserAdapter(Context context,String strUpTo, List<String> storages) {
        inflater = LayoutInflater.from(context);
        this.strUpTo = strUpTo;
        this.storages = storages;
        if(!storages.contains(Environment.getExternalStorageDirectory().getPath())){
            environmentStoragePath = Environment.getExternalStorageDirectory().getPath();
        }



        Resources res = context.getResources();

        //icFile = res.getDrawable(R.drawable.ic_file);
        icFolder = ContextCompat.getDrawable(context, R.drawable.ic_folder);
        icSdCard = ContextCompat.getDrawable(context, R.drawable.ic_external_sdcard);
        icZip = ContextCompat.getDrawable(context, R.drawable.ic_zip);
        icUp = ContextCompat.getDrawable(context, R.drawable.ic_up);
    }

    public void setDirectory(File dir){
        setDirectory(dir, false);
    }

    public void setDirectory(File dir, boolean useRoot) {


        currentDirectory = dir;
        isRootDirectory = currentDirectory.getParentFile() == null;


        Pair<List<File>, List<File>> files = FileUtils.listFiles(dir, useRoot, false, null, new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".zip");
            }
        });

        List<File> folderList = files.first;
        List<File> fileList = files.second;

        int totalSize = 0;

        if (!folderList.isEmpty()) {
            totalSize += folderList.size();
            folderListSize =  totalSize + (isRootDirectory ? 0 : 1);
            Collections.sort(folderList, FILE_NAME_COMPARATOR);
            int sparseSize = totalSize + (isRootDirectory ? 0 : 1);
            isSdCard = new SparseBooleanArray(sparseSize);
            int storageSize = storages.size();
            int i = (isRootDirectory ? 0 : 1);
            int cardCount = 0;
            for (File f : folderList) {
                String path = f.getAbsolutePath();
                if (storages.contains(path) || (environmentStoragePath != null && path.equals(environmentStoragePath))) {
                    isSdCard.append(i, true);
                    cardCount++;
                }
                if (cardCount == storageSize+(environmentStoragePath!=null?1:0)) {
                    break;
                }
                i++;
            }
        }else{
            folderListSize = -1;
        }

        if (!fileList.isEmpty()) {
            totalSize += folderList.size();
            Collections.sort(fileList, FILE_NAME_COMPARATOR);
        }

        this.fileList = new ArrayList<File>(totalSize + (isRootDirectory ? 0 : 1));
        if (!isRootDirectory) {
            this.fileList.add(currentDirectory.getParentFile());
        }
        if (folderList != null) {
            this.fileList.addAll(folderList);
        }
        if (fileList != null) {
            this.fileList.addAll(fileList);
        }
        notifyDataSetChanged();

    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        final ViewHolder holder = new ViewHolder(inflater.inflate(R.layout.list_item_file, parent, false));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File f = fileList.get(holder.getAdapterPosition());
                boolean isFolder = false;
                int i = holder.getAdapterPosition();
                if (f.isDirectory() || f.getName().endsWith(".zip") || (isFolder = i<folderListSize)) {
                    if (listener != null) {
                        listener.onItemClick(v, i, f, isFolder);
                    }
                }
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        File file = fileList.get(position);
        holder.imgIcon.setImageDrawable(icFolder);

        if (position == 0 && !isRootDirectory) {
            holder.imgIcon.setImageDrawable(icUp);
            holder.txtName.setText(strUpTo + " " + (file.getName().equals("") ? "/" : file.getName()));
            holder.txtMetaSize.setVisibility(View.GONE);
            holder.txtMetaDate.setVisibility(View.GONE);
        } else {
            holder.txtMetaSize.setVisibility(View.VISIBLE);
            holder.txtMetaDate.setVisibility(View.VISIBLE);
            
            long fileSize = file.length();

            String size = fileSize>0?formatBytes(fileSize):"";
            long lastModified = file.lastModified();
            String modified = lastModified == 0 ? "" : DATE_FORMAT.format(file.lastModified());

            if (file.isDirectory() || position<folderListSize) {
                size = "";
                if (isSdCard.get(position)) {
                    holder.imgIcon.setImageDrawable(icSdCard);
                } else {
                    holder.imgIcon.setImageDrawable(icFolder);
                }
            } else {

                if (file.getName().endsWith(".zip")) {
                    holder.imgIcon.setImageDrawable(icZip);
                } else {
                    holder.imgIcon.setImageDrawable(icFile);
                }
            }
            holder.txtMetaSize.setText(size);
            holder.txtName.setText(file.getName());
            holder.txtMetaDate.setText(modified);

        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return fileList!=null?fileList.size():0;
    }


    private String formatBytes(long bytes) {
        long k = 1000;

        if (bytes < k) {
            return bytes + " bytes";
        } else if (bytes >= k && bytes < k * k) {
            return DECIMAL_FORMAT.format(bytes / k) + " KB";
        } else if (bytes >= k * k && bytes < k * k * k) {
            return DECIMAL_FORMAT.format(bytes / (k * k)) + " MB";
        } else if (bytes >= k * k * k && bytes < k * k * k * k) {
            return DECIMAL_FORMAT.format(bytes / (k * k * k)) + " GB";
        } else {
            return DECIMAL_FORMAT.format(bytes / (k * k * k * k)) + " TB";
        }
    }

    public void setOnItemClickListener(FileBrowserAdapterClickListener onItemClickListener) {
        this.listener = onItemClickListener;
    }

    public File getItem(int position) {
        return fileList.get(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtName;
        TextView txtMetaSize;
        TextView txtMetaDate;
        ImageView imgIcon;

        public ViewHolder(View view) {
            super(view);

            this.txtName = (TextView) view.findViewById(R.id.txt_file_name);
            this.txtMetaSize = (TextView) view.findViewById(R.id.txt_file_meta_size);
            this.txtMetaDate = (TextView) view.findViewById(R.id.txt_file_meta_date);
            this.imgIcon = (ImageView) view.findViewById(R.id.img_file_icon);
        }
    }

}