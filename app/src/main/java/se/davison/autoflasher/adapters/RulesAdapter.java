package se.davison.autoflasher.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import se.davison.autoflasher.R;
import se.davison.autoflasher.model.FlashRule;

/**
 * Created by richard on 19/01/15.
 */
public class RulesAdapter extends RecyclerView.Adapter<RulesAdapter.ViewHolder> {

    private AdapterView.OnItemClickListener itemClickListener;
    private AdapterView.OnItemLongClickListener itemLongClickListener;

    public void setOnItemClickListener(AdapterView.OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void setOnItemLongClickListener(AdapterView.OnItemLongClickListener itemLongClickListener) {
        this.itemLongClickListener = itemLongClickListener;
    }

    private ArrayList<FlashRule> items = new ArrayList<FlashRule>(10);
    private LayoutInflater inflater;

    public List<FlashRule> getItems() {
        return items;
    }
    public RulesAdapter(Context context){
        inflater = LayoutInflater.from(context);

        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorAccent,typedValue,true);
        accentColor = typedValue.data;
    }
    private SparseBooleanArray selectedItems = new SparseBooleanArray(100);

    private boolean selectionMode = false;
    private int accentColor;

    public void setSelectionMode(boolean selectionMode) {
        this.selectionMode = selectionMode;
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public void setItemSelected(int index, boolean selected){
        selectedItems.put(index,selected);
        notifyItemChanged(index);
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflate = inflater.inflate(R.layout.list_item_flash_rule, parent, false);

        final ViewHolder holder = new ViewHolder(inflate);
        if(itemClickListener!=null) {
            inflate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position =holder.getPosition();

                    if(selectionMode) {
                        selectedItems.put(position, !selectedItems.get(position));

                    }
                    itemClickListener.onItemClick(null, v, position, holder.getItemId());
                    if(selectionMode){
                        notifyItemChanged(position);
                    }

                }
            });
        }
        if(itemLongClickListener!=null){
            inflate.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if(!selectionMode) {
                        return itemLongClickListener.onItemLongClick(null, v, holder.getAdapterPosition(), holder.getItemId());
                    }
                    return false;
                }
            });
        }
        return holder;
    }

    public ArrayList<FlashRule> getSelectedItems() {
        ArrayList<FlashRule> newRules = new ArrayList<FlashRule>(getItemCount());
        int key = 0;
        for(int i = 0; i < selectedItems.size(); i++) {
            key = selectedItems.keyAt(i);
            // get the object by the key.
            if(selectedItems.get(key)){
                newRules.add(items.get(key));
            }
        }
        return newRules;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FlashRule rule = items.get(position);
        if(selectedItems.get(position)){
            holder.itemView.setBackgroundColor(accentColor&0x33FFFFFF);
        }else{
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
        }
        holder.text1.setText(rule.getMatchPhrase());
        holder.text2.setText(rule.getFile().getParent());
    }

    @Override
    public int getItemCount() {
        return items==null?0:items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setItems(ArrayList items) {
        this.items = items;
    }

    public boolean inSelectionMode() {
        return selectionMode;
    }

    public int getSelectionCount() {
        int key = 0;
        int size = 0;
        for(int i = 0; i < selectedItems.size(); i++) {
            key = selectedItems.keyAt(i);
            if(selectedItems.get(key)){
                size++;
            }
        }
        return size;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView text1;
        TextView text2;


        public ViewHolder(View itemView) {
            super(itemView);
            text1 = (TextView) itemView.findViewById(android.R.id.text1);
            text2 = (TextView) itemView.findViewById(android.R.id.text2);
        }
    }
}
