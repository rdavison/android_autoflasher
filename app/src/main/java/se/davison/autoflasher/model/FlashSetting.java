package se.davison.autoflasher.model;

public class FlashSetting {
	
	private String prefKey;
	private String name;
	
	@Override
	public String toString() {
		return name;
	}

	public FlashSetting(String prefKey, String name) {
		super();
		this.prefKey = prefKey;
		this.name = name;
	}
	
	public String getPrefKey() {
		return prefKey;
	}
	
	public void setPrefKey(String prefKey) {
		this.prefKey = prefKey;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
