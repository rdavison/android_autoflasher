package se.davison.autoflasher.model;

import java.io.File;
import java.io.FilenameFilter;
import java.io.Serializable;
import java.util.regex.PatternSyntaxException;

import static se.davison.autoflasher.model.FlashRule.NameRule.BEGINS_WITH;

public class FlashRule implements Serializable, FilenameFilter {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3025855777441816534L;

	@Override
	public boolean accept(File dir, String filename) {
		if (filename == null || !filename.endsWith(".zip")) {
			return false;
		}

		switch (nameRule) {
			case BEGINS_WITH:
				return filename.startsWith(matchPhrase);
			case ENDS_WITH:
				return filename.endsWith(matchPhrase);
			case IS_EXACTLY:
				return filename.equals(matchPhrase);
			case CONTAINS:
				return filename.contains(matchPhrase);
			case REGEXP:
				boolean result = false;

				try{
					result = filename.matches(matchPhrase);
				}catch (PatternSyntaxException e){
					e.printStackTrace();
				}

				return result;
		}
		return false;
	}

	public enum NameRule{
		BEGINS_WITH, ENDS_WITH, IS_EXACTLY, CONTAINS, REGEXP
	}
	
	private String matchPhrase;
	private File file;
	private NameRule nameRule;
	
	public FlashRule(File file, String matchPhrase, NameRule nameRule) {
		super();
		this.file = file;
		this.matchPhrase = matchPhrase;
		this.nameRule = nameRule;
	}
	
	@Override
	public String toString() {
		return matchPhrase;
	}
	
	public String getMatchPhrase() {
		return matchPhrase;
	}
	public void setMatchPhrase(String matchPhrase) {
		this.matchPhrase = matchPhrase;
	}
	public File getFile() {
		return file;
	}
	public void setFile(File file) {
		this.file = file;
	}
	public NameRule getNameRule() {
		return nameRule;
	}
	public void setNameRule(NameRule nameMatch) {
		this.nameRule = nameMatch;
	}


}
