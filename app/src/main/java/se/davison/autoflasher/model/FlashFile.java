package se.davison.autoflasher.model;

import java.io.File;

public class FlashFile extends File {
	private static final long serialVersionUID = -804102947894626973L;

	public FlashFile(String path) {
		super(path);
	}
	
	public FlashFile(File file){
		super(file.getAbsolutePath());
	}
	
	@Override
	public String toString() {
		return super.getPath();
	}

}
